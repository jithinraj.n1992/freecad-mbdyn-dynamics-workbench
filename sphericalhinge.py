# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################

import FreeCAD
import Draft

class Sphericalhinge:
    def __init__(self, obj, label, node1, node2):
        
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)
        
        #Get the joint´s absolute position:
        x = node1.absolute_position_X
        y = node1.absolute_position_Y
        z = node1.absolute_position_Z
        
        #Calculate the relative offset:            
        x1 = x - node2.absolute_position_X
        y1 = y - node2.absolute_position_Y
        z1 = z - node2.absolute_position_Z

        obj.addExtension("App::GroupExtensionPython", self)          
        
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","spherical hinge","label",1).label = label        
        obj.addProperty("App::PropertyString","node 1","spherical hinge","node 1",1).node_1 = node2.label
        obj.addProperty("App::PropertyString","node 2","spherical hinge","node 2",1).node_2 = node1.label
        obj.addProperty("App::PropertyString","joint","spherical hinge","joint",1).joint = 'spherical hinge'

        #Absolute position:
        obj.addProperty("App::PropertyString","absolute position X","absolute position","absolute position X",1).absolute_position_X = str(round(x.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Y","absolute position","absolute position Y",1).absolute_position_Y = str(round(y.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Z","absolute position","absolute position Z",1).absolute_position_Z = str(round(z.getValueAs('m').Value,precission))+" m"
        
        #Relative offset:          
        obj.addProperty("App::PropertyString","relative offset X","relative offset","relative offset X",1).relative_offset_X = str(round(x1.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset Y","relative offset","relative offset Y",1).relative_offset_Y = str(round(y1.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset Z","relative offset","relative offset Z",1).relative_offset_Z = str(round(z1.getValueAs('m').Value,precission))+" m"
        
        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","animation","frame")
        obj.frame=['global','local']        
        
        obj.addProperty("App::PropertyString","force vector multiplier","animation","force vector multiplier").force_vector_multiplier = '1'
        
        obj.addProperty("App::PropertyString","structural dummy","animation","structural dummy").structural_dummy = '1'

        obj.Proxy = self
        
        #Add the coordinate system and an system to the GUI. The coordinate system represents the position of the node in space:
        #length = FreeCAD.ActiveDocument.getObjectsByLabel("x: structural: " + node1.label)[0].Length.Value # Calculate the body characteristic length. Will be used to size the arrows that represent the node.
        p1 = FreeCAD.Vector(0, 0, 0)

        #Add the vector to visualize reaction forces
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        p2 = FreeCAD.Vector(Llength, Llength, Llength)    
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        #l.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        d.Label = "jf: "+ label                                                   
        
    def execute(self, fp):
       
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)
        #Get the nodes
        node1 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_2)[0]
        node2 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_1)[0] 
        
        #Get the new absolute position:
        x = node1.absolute_position_X
        y = node1.absolute_position_Y
        z = node1.absolute_position_Z
        
        #Calculate the new relative offset:            
        x1 = node1.absolute_position_X - node2.absolute_position_X
        y1 = node1.absolute_position_Y - node2.absolute_position_Y
        z1 = node1.absolute_position_Z - node2.absolute_position_Z
        
        #Update the absolute position:
        fp.absolute_position_X = str(round(x.getValueAs('m').Value,precission))+" m"
        fp.absolute_position_Y = str(round(y.getValueAs('m').Value,precission))+" m"
        fp.absolute_position_Z = str(round(z.getValueAs('m').Value,precission))+" m"
                        
        #Update the offset:
        fp.relative_offset_X = str(round(x1.getValueAs('m').Value,precission))+" m"
        fp.relative_offset_Y = str(round(y1.getValueAs('m').Value,precission))+" m"
        fp.relative_offset_Z = str(round(z1.getValueAs('m').Value,precission))+" m"
        
        #Move the arrows to the new position:
        #FreeCAD.ActiveDocument.getObjectsByLabel('x: joint: '+fp.label)[0].Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))                     
        #FreeCAD.ActiveDocument.getObjectsByLabel('y: joint: '+fp.label)[0].Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))                     
        #FreeCAD.ActiveDocument.getObjectsByLabel('z: joint: '+fp.label)[0].Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))                     
        FreeCAD.ActiveDocument.getObjectsByLabel('jf: '+fp.label)[0].Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))                             
        
        FreeCAD.Console.PrintMessage("SPHERICAL HINGE JOINT: " +fp.label+" successful recomputation...\n")               
               
