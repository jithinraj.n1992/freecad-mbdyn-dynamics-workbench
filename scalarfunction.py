# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
#edge.discretize

import FreeCAD
import numpy as np

class ScalarFunction:
    def __init__(self, obj, label, steps, Yvalues = "None", Xvalues = "None"): 
        
        obj.addExtension("App::GroupExtensionPython", self)
                
        obj.addProperty("App::PropertyString","label","scalar function","label",1).label = label
        obj.addProperty("App::PropertyString","independent variable","scalar function","independent variable").independent_variable = "Time"
        
        obj.addProperty("App::PropertyString","initial value","domain","initial value").initial_value = FreeCAD.ActiveDocument.getObject("MBDyn").initial_time[:-2]
        obj.addProperty("App::PropertyString","final value","domain","final value").final_value = FreeCAD.ActiveDocument.getObject("MBDyn").final_time[:-2]
        
        #function type
        obj.addProperty("App::PropertyEnumeration","type","scalar function","type")
        obj.type=['cubicspline: cubic natural spline interpolation between the set of points (insert the points in the text file within this object)',
                  'multilinear: multilinear interpolation between the set of points (insert the points in the text file within this object)',
                  'chebychev: Chebychev interpolation between the set of points (insert the points in the text file within this object)']
  
        obj.addProperty("App::PropertyEnumeration","extrapolation","scalar function","extrapolation")
        obj.extrapolation=['do not extrapolate', 'extrapolate']
                
        obj.Proxy = self

        obj = FreeCAD.ActiveDocument.addObject("App::TextDocument", "points_"+ label)   
        obj.Label = 'points: '+ label
        
        #Add an initial scalar function. This is assumed to be a function of time.
        InitialTime = float(FreeCAD.ActiveDocument.getObject("MBDyn").initial_time[:-2])
        FinalTime = float(FreeCAD.ActiveDocument.getObject("MBDyn").final_time[:-2])
        time = np.linspace(InitialTime, FinalTime, num=steps)    
        
        if Yvalues == "None" and Xvalues == "None":
            for i in time:    
                obj.Text = obj.Text + str(i)+ ',   0.0, \n'
            
        if  Yvalues != "None" and Xvalues == "None":   
            aux = 0
            for i in Yvalues:    
                obj.Text = obj.Text + str(time[aux])+ ',   '+str(i)+', \n'
                aux = aux + 1
                
        if  Xvalues != "None" and Yvalues != "None":   
            aux = 0
            for i in Xvalues:    
                obj.Text = obj.Text + str(Xvalues[aux])+ ',   '+str(Yvalues[aux])+', \n'
                aux = aux + 1
            
        obj.Text = obj.Text[:-3]#Remove the last comma
                               
        FreeCAD.ActiveDocument.recompute()

    def execute(self, fp):
        obj = FreeCAD.ActiveDocument.getObjectsByLabel("points: "+fp.label)[0].Text.replace(" ","").replace("\n","").split(",")
        points = FreeCAD.ActiveDocument.getObjectsByLabel("points: "+fp.label)[0]
        steps = int(len(obj)/2)
        InitialValue = float(fp.initial_value)
        FinalValue = float(fp.final_value)
        time = np.linspace(InitialValue, FinalValue, num=steps) 
        Yvalues = obj[1::2]
        
        points.Text = ""
        aux = 0
        for i in Yvalues:    
            points.Text = points.Text + str(time[aux])+ ',   '+str(i)+', \n'
            aux = aux + 1    
        
        points.Text = points.Text[:-3]#Remove the last comma
        
        FreeCAD.Console.PrintMessage("SCALAR FUNCTION: " +fp.label+" successful recomputation...\n") 
        pass
            
            
                   
