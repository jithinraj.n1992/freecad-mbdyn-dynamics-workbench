# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################

import FreeCAD, FreeCADGui
from sympy import Point3D, Line3D
import Draft

class StructuralForce:
    def __init__(self, obj, label, node):
     
        obj.addExtension("App::GroupExtensionPython", self)  
        
        #Calculate the absolute position:       
        x = node.absolute_position_X
        y = node.absolute_position_Y
        z = node.absolute_position_Z
        
        #Calculate the offset:         
        try:
            X = FreeCAD.Units.Quantity(FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].Curve.Center[0],FreeCAD.Units.Unit('mm'))
            Y = FreeCAD.Units.Quantity(FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].Curve.Center[1],FreeCAD.Units.Unit('mm'))
            Z = FreeCAD.Units.Quantity(FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].Curve.Center[2],FreeCAD.Units.Unit('mm'))
            x11 = X-x
            y11 = Y-y
            z11 = Z-z
            reference = str(x11.getValueAs('m'))+", "+str(y11.getValueAs('m'))+", "+str(z11.getValueAs('m'))
        except:
            try:
                X = FreeCAD.Units.Quantity(FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].Point[0],FreeCAD.Units.Unit('mm'))
                Y = FreeCAD.Units.Quantity(FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].Point[1],FreeCAD.Units.Unit('mm'))
                Z = FreeCAD.Units.Quantity(FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].Point[2],FreeCAD.Units.Unit('mm'))
                x11 = X-x
                y11 = Y-y
                z11 = Z-z
                reference = str(x11.getValueAs('m'))+", "+str(y11.getValueAs('m'))+", "+str(z11.getValueAs('m'))
            except:
                try:
                    X = FreeCAD.Units.Quantity(FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].CenterOfMass[0],FreeCAD.Units.Unit('mm'))
                    Y = FreeCAD.Units.Quantity(FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].CenterOfMass[1],FreeCAD.Units.Unit('mm'))
                    Z = FreeCAD.Units.Quantity(FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].CenterOfMass[2],FreeCAD.Units.Unit('mm'))
                    x11 = X-x
                    y11 = Y-y
                    z11 = Z-z
                    reference = str(x11.getValueAs('m'))+", "+str(y11.getValueAs('m'))+", "+str(z11.getValueAs('m'))
                except: 
                    X = x
                    Y = y
                    Z = z
                    reference = "null"
        
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","structural force","label",1).label = label
        obj.addProperty("App::PropertyString","force","structural force","force",1).force = 'structural force'
        obj.addProperty("App::PropertyString","node","structural force","node",1).node = node.label
        obj.addProperty("App::PropertyString","force value","structural force","force value").force_value = "none" 
        obj.addProperty("App::PropertyString","modifier","structural force","modifier").modifier = "1.0"
        
        #obj.addProperty("App::PropertyString","type","Structural force","type").type = 'absolute'
        obj.addProperty("App::PropertyEnumeration","type","structural force","type")
        obj.type=['absolute','follower']
        
        
        obj.addProperty("App::PropertyString","relative_arm","structural force","relative_arm",1).relative_arm = reference

        #Force parameters        

        obj.addProperty("App::PropertyEnumeration","force type","force parameters","force type")
        obj.force_type=['single','absolute','follower']

        obj.addProperty("App::PropertyString","direction","force parameters","direction").direction = '0, 0, 1'
        
        obj.Proxy = self          

        #Add the coordinate system and an system to the GUI. The coordinate system represents the position of the node in space:
        length = FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4
        p1 = FreeCAD.Vector(0, 0, 0)
        #Add Z vector of the coordinate system:
        p2 = FreeCAD.Vector(length, 0, 0)
        p2 = FreeCAD.Vector(0, 0, length)
        l = Draft.makeLine(p1, p2)
        l.Label = 'z: force: '+ label
        l.ViewObject.ArrowType = u"Arrow"            
        l.ViewObject.LineColor = (1.00,1.00,1.00)
        l.ViewObject.PointColor = (1.00,1.00,1.00)
        l.Placement=FreeCAD.Placement(FreeCAD.Vector(X.Value,Y.Value,Z.Value), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
            
        l.ViewObject.EndArrow = True        
        l.ViewObject.LineWidth = 2.00
        l.ViewObject.PointSize = 2.00
        l.ViewObject.ArrowSize = str(length/30)+' mm' 
        #l.ViewObject.Selectable = False     
        
        FreeCAD.ActiveDocument.recompute()
        
    def execute(self, fp):
        '''Do something when doing a recomputation, this method is mandatory'''
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)#Max number of decimal places
        
        ##############################################################################Calculate the new orientation: 
        ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: force: "+fp.label)[0]#get the force´s Z line        
        #Two 3D points that define the joint´s line:
        p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2]) 
        l1 = Line3D(p1, p2)#Line that defines the joint
        #generate the orientation matrix (it has to be a unitary vector, so that it does not alter the force magnitude):
        ar = ZZ.Length.Value     
        fp.direction = str(round(l1.direction[0]/ar,precission)) +", "+ str(round(l1.direction[1]/ar,precission)) +", "+ str(round(l1.direction[2]/ar,precission))
        
        #Change the placement of the foce´s Z line (in the case it´s node has been moved):
        node = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node)[0]
        #Calculate the absolute position:       
        x = node.absolute_position_X
        y = node.absolute_position_Y
        z = node.absolute_position_Z
        #ZZ.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        
        FreeCAD.ActiveDocument.getObjectsByLabel("force: "+fp.label)[0].Visibility = False
        FreeCAD.ActiveDocument.getObjectsByLabel("force: "+fp.label)[0].Visibility = True

        FreeCAD.Console.PrintMessage("STRUCTURAL FORCE: " +fp.label+" successful recomputation...\n")