# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################


import FreeCAD

class Drive:
    def __init__(self, obj, label): 
        
        obj.addExtension("App::GroupExtensionPython", self)
        
        obj.addProperty("App::PropertyString","label","drive","label",1).label = label
        
        #function type
        obj.addProperty("App::PropertyEnumeration","type","drive","type")
        obj.type=['const, <const_coef>',
                  'cosine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>',
                  'cosine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>',
                  'cosine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>',
                  'cosine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>',                              
                  'cubic, <const_coef>, <linear_coef>, <parabolic_coef>, <cubic_coef>',
                  'direct',
                  'double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, forever, <initial_value>',
                  'double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, <d_final_time>, <initial_value>',
                  'double step, <initial_time>, <final_time>, <step_value>, <initial_value>',
                  'linear, <const_coef>, <slope_coef>',
                  'ramp, <slope>, <initial_time>, forever, <initial_value>',
                  'ramp, <slope>, <initial_time>, <final_time>, <initial_value>',
                  'sine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>',
                  'sine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>',
                  'sine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>',
                  'sine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>',
                  'step, <initial_time>, <step_value>, <initial_value>',
                  'string',
                  'time',
                  'timestep',
                  'unit']
                  

        #function parameters:
        obj.addProperty("App::PropertyString","a_slope","drive parameters","a_slope").a_slope = '0.1'
        obj.addProperty("App::PropertyString","string","drive parameters","string").string = '1.5*sin(2.*Time)'        
        obj.addProperty("App::PropertyString","slope","drive parameters","slope").slope = '0.1'
        obj.addProperty("App::PropertyString","slope_coef","drive parameters","slope_coef").slope_coef = '0.1'
        obj.addProperty("App::PropertyString","d_slope","drive parameters","d_slope").d_slope = '0.1'
        obj.addProperty("App::PropertyString","a_initial_time","drive parameters","a_initial_time").a_initial_time = '0.'
        obj.addProperty("App::PropertyString","d_initial_time","drive parameters","d_initial_time").d_initial_time = '1.'
        obj.addProperty("App::PropertyString","a_final_time","drive parameters","a_final_time").a_final_time = '1.'
        obj.addProperty("App::PropertyString","d_final_time","drive parameters","d_final_time").d_final_time = '2.'        
        obj.addProperty("App::PropertyString","const_coef","drive parameters","const_coef").const_coef = '10.'
        obj.addProperty("App::PropertyString","step_value","drive parameters","step_value").step_value = '10.'
        obj.addProperty("App::PropertyString","linear_coef","drive parameters","linear_coef").linear_coef = '1.'
        obj.addProperty("App::PropertyString","parabolic_coef","drive parameters","parabolic_coef").parabolic_coef = '1.'
        obj.addProperty("App::PropertyString","cubic_coef","drive parameters","cubic_coef").cubic_coef = '1.'        
        obj.addProperty("App::PropertyString","initial_time","drive parameters","initial_time").initial_time = '0.'
        obj.addProperty("App::PropertyString","final_time","drive parameters","final_time").final_time = '1.'
        obj.addProperty("App::PropertyString","angular_vel","drive parameters","angular_vel").angular_vel = '10.'
        obj.addProperty("App::PropertyString","amplitude","drive parameters","amplitude").amplitude = '10.'
        obj.addProperty("App::PropertyString","initial_value","drive parameters","initial_value").initial_value = '0.'
        obj.addProperty("App::PropertyString","number_of_cycles","drive parameters","number_of_cycles").number_of_cycles = '1.' 
        
        
        obj.addProperty("App::PropertyString","expression","drive","expression",1).expression = '1.5*sin(2.*Time)'
        
        obj.Proxy = self      
                               
        FreeCAD.ActiveDocument.recompute()

    def execute(self, fp):
        if(fp.type == 'const, <const_coef>'):
            fp.expression = 'const, '+fp.const_coef
                        
        if(fp.type == 'cosine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>'):
            fp.expression='cosine, '+fp.initial_time+', '+fp.angular_vel+', '+fp.amplitude+', half, '+fp.initial_value

        if(fp.type == 'cosine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>'):
            fp.expression = 'cosine, '+fp.initial_time+', '+fp.angular_vel+', '+fp.amplitude+', one, '+fp.initial_value

        if(fp.type == 'cosine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>'):
            fp.expression = 'cosine, '+fp.initial_time+', '+fp.angular_vel+', '+fp.amplitude+', forever, '+fp.initial_value

        if(fp.type == 'cosine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>'):
            fp.expression = 'cosine, '+fp.initial_time+', '+fp.angular_vel+', '+fp.amplitude+', '+fp.number_of_cycles+', '+fp.initial_value

        if(fp.type == 'cubic, <const_coef>, <linear_coef>, <parabolic_coef>, <cubic_coef>'):
            fp.expression = 'cubic, '+fp.const_coef+', '+fp.linear_coef+', '+fp.parabolic_coef+', '+fp.cubic_coef

        if(fp.type == 'direct'):
            fp.expression = 'direct'

        if(fp.type == 'double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, forever, <initial_value>'):
            fp.expression = 'double ramp, '+fp.a_slope+', '+fp.a_initial_time+', '+fp.a_final_time+', '+fp.d_slope+', '+fp.d_initial_time+', forever, '+fp.initial_value

        if(fp.type == 'double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, <d_final_time>, <initial_value>'):
            fp.expression = 'double ramp, '+fp.a_slope+', '+fp.a_initial_time+', '+fp.a_final_time+', '+fp.d_slope+', '+fp.d_initial_time+', '+fp.d_final_time+', '+fp.initial_value

        if(fp.type == 'double step, <initial_time>, <final_time>, <step_value>, <initial_value>'):
            fp.expression = 'double step, '+fp.initial_time+', '+fp.final_time+', '+fp.step_value+', '+fp.initial_value
            
        if(fp.type == 'linear, <const_coef>, <slope_coef>'):
            fp.expression = 'linear, '+fp.const_coef+', '+fp.slope_coef

        if(fp.type == 'ramp, <slope>, <initial_time>, forever, <initial_value>'):
            fp.expression = 'ramp, '+fp.slope+', '+fp.initial_time+', forever, '+fp.initial_value

        if(fp.type == 'ramp, <slope>, <initial_time>, <final_time>, <initial_value>'):
            fp.expression = 'ramp, '+fp.slope+', '+fp.initial_time+', '+fp.final_time+', '+fp.initial_value

        if(fp.type == 'sine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>'):
            fp.expression = 'sine, '+fp.initial_time+', '+fp.angular_vel+', '+fp.amplitude+', half, '+fp.initial_value

        if(fp.type == 'sine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>'):
            fp.expression = 'sine, '+fp.initial_time+', '+fp.angular_vel+', '+fp.amplitude+', one, '+fp.initial_value

        if(fp.type == 'sine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>'):
            fp.expression = 'sine, '+fp.initial_time+', '+fp.angular_vel+', '+fp.amplitude+', forever, '+fp.initial_value

        if(fp.type == 'sine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>'):
            fp.expression = '           sine, '+fp.initial_time+', '+fp.angular_vel+', '+fp.amplitude+', '+fp.number_of_cycles+', '+fp.initial_value
        
        if(fp.type == 'step, <initial_time>, <step_value>, <initial_value>'):
            fp.expression = 'step, '+fp.initial_time+', '+fp.step_value+', '+fp.initial_value

        if(fp.type == 'string'):
            fp.expression = 'string, "'+fp.string+'"'

        if(fp.type == 'time'):
            fp.expression = 'time'

        if(fp.type == 'timestep'):
            fp.expression = 'timestep'
            
        if(fp.type == 'unit'):
            fp.expression = 'unit'     
            
        FreeCAD.Console.PrintMessage("DRIVE: " +fp.label+ " successful recomputation...\n")
            
            
                   
