# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
This class implements an in-plane joint between two nodes. It receives an integer number which is used to label the joint, and two nodes. The second node is then constrained to a line fixed to the first node.

An in-line joint forces a point relative to the second node to move along a line attached to the first node.
A point, optionally offset by "relative offset" from the position of "node 2", slides along a
line that passes through a point that is rigidly offset by "relative_line_position" from the position of
"node 1", and is directed as direction 3 (the Z axis) of "relative orientation". 

The joint is defined as:

     joint: 2, #joint label
            in plane,
            2, # first asociated node
                position, 0., 0., 0., #relative plane position
                0., 0., 1., #relative normal direction
            1, # second asociated node
                offset, 0.0, 0.0, 0.0; # relatove offset

'''

#from FreeCAD.Units.Units import FreeCAD.Units.Unit,FreeCAD.Units.Quantity
import FreeCAD
import Draft
from sympy import Point3D, Line3D

class Inplane:
    def __init__(self, obj, label, node1, node2):
        
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)

        x = node1.absolute_position_X
        y = node1.absolute_position_Y
        z = node1.absolute_position_Z
     
        obj.addExtension("App::GroupExtensionPython", self)  
        
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","in plane joint","label").label = label
        obj.addProperty("App::PropertyString","joint","in plane joint","joint").joint = 'in plane'
        obj.addProperty("App::PropertyString","node 1","in plane joint","node 1").node_1 = node1.label
        obj.addProperty("App::PropertyString","node 2","in plane joint","node 2").node_2 = node2.label
        
        #The absolute position is set at the node 1 position, only for animation, not for MBDyn sumulation:        
        obj.addProperty("App::PropertyString","absolute position X","absolute position","absolute position X",1).absolute_position_X = str(round(x.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Y","absolute position","absolute position Y",1).absolute_position_Y = str(round(y.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Z","absolute position","absolute position Z",1).absolute_position_Z = str(round(z.getValueAs('m').Value,precission))+" m" 

        #The relative plane position is initially set to (0,0,0), this is, the line passes through node 1
        obj.addProperty("App::PropertyString","relative plane position X","relative plane position","relative plane position X").relative_plane_position_X = "0. m"
        obj.addProperty("App::PropertyString","relative plane position Y","relative plane position","relative plane position Y").relative_plane_position_Y = "0. m"
        obj.addProperty("App::PropertyString","relative plane position Z","relative plane position","relative plane position Z").relative_plane_position_Z = "0. m"
        
        #The relative plane orientation is initially set to (0,0,1), this is, the X-Y plane.
        obj.addProperty("App::PropertyString","relative normal direction","relative normal direction","relative normal direction").relative_normal_direction = '0., 0., 1.'
        
        
        #The relative offset is initially set to (0,0,0), this is, node 2 is at the plane, without offset
        obj.addProperty("App::PropertyString","relative offset X","node offset relative to plane","relative offset X").relative_offset_X = "0. m"
        obj.addProperty("App::PropertyString","relative offset Y","node offset relative to plane","relative offset Y").relative_offset_Y = "0. m"
        obj.addProperty("App::PropertyString","relative offset Z","node offset relative to plane","relative offset Z").relative_offset_Z = "0. m"
        
        
        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","animation","frame")
        obj.frame=['global','local']  
        
        obj.addProperty("App::PropertyString","structural dummy","animation","structural dummy").structural_dummy = ''
        
        obj.addProperty("App::PropertyString","force vector multiplier","animation","force vector multiplier").force_vector_multiplier = '1'

        obj.Proxy = self
        
        length = FreeCAD.ActiveDocument.getObjectsByLabel("x: structural: " + node1.label)[0].Length.Value # Calculate the body characteristic length. Will be used to size the arrows that represent the node.
        p1 = FreeCAD.Vector(0, 0, 0)

        #Add z vector of the coordinate system:
        p2 = FreeCAD.Vector(0, 0, 2*length)
        l = Draft.makeLine(p1, p2)
        l.Label = 'z: joint: '+ label
        #l.ViewObject.ArrowType = u"Dot"            
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)
        l.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        #l.ViewObject.EndArrow = True        
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        #l.ViewObject.DrawStyle = u"Dashed"
        #l.ViewObject.ArrowSize = str(length/30)+' mm' 
        #l.ViewObject.Selectable = False             
        
        #Add the vector to visualize reaction forces        
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        p1 = FreeCAD.Vector(x, y, z)
        p2 = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)         
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        #d.ViewObject.Selectable = False
        d.Label = "jf: "+ label                    
                              
    def execute(self, fp):
        '''Do something when doing a recomputation, this method is mandatory'''
        ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+fp.label)[0]#get the joint´s line
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)#Max number of decimal places
        #Two 3D points that define the joint´s line:
        p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2]) 
        #A line that defines the joint:
        l1 = Line3D(p1, p2)#Line that defines the joint
        #generate the orientation matrix:   
        #fp.relative_normal_direction = str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) +", "+ str(round(l1.direction[2],precission))
        
        FreeCAD.Console.PrintMessage("IN-PLANE JOINT: " +fp.label+" successful recomputation...\n")                                  
                                  