# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################

import FreeCAD

class Abstractnode: 
    def __init__(self, obj, Label):
            
        obj.addProperty("App::PropertyString","type","abstract node","type",1).type = 'abstract'
        obj.addProperty("App::PropertyString","label","abstract node","label",1).label = Label
        obj.addProperty("App::PropertyString","initial value","abstract node","initial value",1).initial_value = "0.0"
        obj.addProperty("App::PropertyString","derivative initial value","abstract node","derivative initial value",1).derivative_initial_value = "0.0"

        obj.addProperty("App::PropertyEnumeration","additional_args","abstract node","additional_args")
        obj.additional_args=['algebraic','differential']        

    def execute(self, fp):        
        FreeCAD.Console.PrintMessage("ABSTRACT NODE: " +fp.label+" successful recomputation...\n")   