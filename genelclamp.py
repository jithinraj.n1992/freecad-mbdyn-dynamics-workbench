# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################

#from FreeCAD import Units

import FreeCAD

class Genelclamp:
    def __init__(self, obj, label, node):
        
        obj.addExtension("App::GroupExtensionPython", self)  
        
        obj.addProperty("App::PropertyString","label","genel clamp","label",1).label = label
        obj.addProperty("App::PropertyString","genel","genel clamp","genel",1).genel = "clamp"
        obj.addProperty("App::PropertyString","clamped node","genel clamp","clamped node",1).clamped_node = node.Label
        obj.addProperty("App::PropertyString","imposed value","genel clamp","imposed value").imposed_value = "Please provide a drive"

        
    def execute(self, fp):
       
        FreeCAD.Console.PrintMessage("GENEL CLAMP: " +fp.label+" successful recomputation...\n")                 