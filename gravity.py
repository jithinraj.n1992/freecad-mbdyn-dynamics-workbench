# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################

import FreeCAD
import Draft
from sympy import Point3D, Line3D

class Gravity:
    def __init__(self, obj, BaseBody, label):
        
        obj.addExtension("App::GroupExtensionPython", self)
        
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)
        
        if(BaseBody=="NONE"):#If no body was selected, a uniform gravity is created:
            #gravity type
            obj.addProperty("App::PropertyString","type","gravity","type",1).type="uniform"
            obj.addProperty("App::PropertyString","direction","gravity","direction",1).direction = ''
            obj.addProperty("App::PropertyString","gravity_acceleration","gravity","gravity_acceleration").gravity_acceleration = ""
            length = FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].Length.Value 
            p1 = FreeCAD.Vector(0, 0, 0)
    
            #Add z vector:
            p2 = FreeCAD.Vector(0, length/2, 0)
            l = Draft.makeLine(p1, p2)
            l.Label = 'z: gravity: '+ label
            l.ViewObject.ArrowType = u"Arrow"
            l.ViewObject.ArrowSize = str(length/100)+' mm'
            l.ViewObject.EndArrow = True
            l.ViewObject.LineColor = (1.00,1.00,1.00)
            l.ViewObject.PointColor = (1.00,1.00,1.00)   
            l.ViewObject.LineWidth = 2.00
            l.ViewObject.PointSize = 2.00
            
        else:#If a body was selected, a central gravity is created at the center of mass of the body:        
            #Compute absolute center of mass, relative to global frame, in mm:
            cmx = FreeCAD.Units.Quantity(BaseBody.Shape.Solids[0].CenterOfMass[0],FreeCAD.Units.Unit('mm'))
            cmy = FreeCAD.Units.Quantity(BaseBody.Shape.Solids[0].CenterOfMass[1],FreeCAD.Units.Unit('mm'))
            cmz = FreeCAD.Units.Quantity(BaseBody.Shape.Solids[0].CenterOfMass[2],FreeCAD.Units.Unit('mm'))
            obj.addProperty("App::PropertyString","type","gravity","type",1).type="central"        
            obj.addProperty("App::PropertyString","origin","gravity","origin",1).origin = str(round(cmx.getValueAs('m').Value,precission))+', '+str(round(cmy.getValueAs('m').Value,precission))+', '+str(round(cmz.getValueAs('m').Value,precission))
            obj.addProperty("App::PropertyString","G","gravity","G").G = 'si [m^3*kg^−1*s^−2]'
            obj.addProperty("App::PropertyString","mass","gravity","mass").mass = '5.972e24 [kg]'
        
        #These properties are common to both, central and uniform gravities
        obj.addExtension("App::GroupExtensionPython", self)                                           
        obj.addProperty("App::PropertyString","label","gravity","label",1).label = label

        
        obj.Proxy = self
        
    def execute(self, fp):
        '''TODO: Central gravities: recalculate the origin if the refernce body has been moved'''
        if(fp.type=="uniform"):
            precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)#Max number of decimal places
            
            ##############################################################################Calculate the new orientation: 
            ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: gravity: "+fp.label)[0]#get the joint´s line        
            #Two 3D points that define the joint´s line:
            p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2]) 
            l1 = Line3D(p1, p2)#Line that defines the joint
            magnitude = (l1.direction[0]**2+l1.direction[1]**2+l1.direction[2]**2)**0.5#Calculate the vector´s magnitude
            #generate the new orientation matrix:
            fp.direction = str(round(l1.direction[0]/magnitude,precission)) +", "+ str(round(l1.direction[1]/magnitude,precission)) + ", " + str(round(l1.direction[2]/magnitude,precission))
            
            FreeCAD.ActiveDocument.getObjectsByLabel("gravity: "+fp.label)[0].Visibility = False
            FreeCAD.ActiveDocument.getObjectsByLabel("gravity: "+fp.label)[0].Visibility = True
            
            FreeCAD.Console.PrintMessage("GRAVITY: " +fp.label+ " successful recomputation...\n")