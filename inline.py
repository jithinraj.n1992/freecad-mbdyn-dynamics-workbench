# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
8.12.25 In line

This joint forces a point relative to the second node to move along a line attached to the first node.

<joint_type> ::= in line
    <joint_arglist> ::=
        <node_1_label> ,
            [ position , (Vec3) <relative_line_position> , ]
            [ orientation , (OrientationMatrix) <relative_orientation> , ]
        <node_2_label>
            [ , offset , (Vec3) <relative_offset> ]
            
A point, optionally offset by relative_offset from the position of node node_2_label, slides along a
line that passes through a point that is rigidly offset by relative_line_position from the position of
node_1_label, and is directed as direction 3 of relative_orientation. The joint is shown in Figure 8.9
where the origin and orientation of node_1 are given by axis 1′, 2′, 3′ and relative_line_position and
relative_orientation are the transformations that yield axis 1′
0, 2′
0, 3′
0 when applied to its coordinate
axis. The origin and orientation of node_2 are given by axis 1, 2, 3 and axis 10, 20, 30 is the result of
applying the relative_offset translation to its coordinate axis.

'''

import FreeCAD
import Draft
from sympy import Point3D, Line3D

class Inline:
    def __init__(self, obj, label, node1, node2, reference, reference1):
        
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)
        
        #Get the reference's center of mass, to define the orientation line:
            
        try:
            x4 = FreeCAD.Units.Quantity(reference.Curve.Center[0],FreeCAD.Units.Unit('mm'))  
            y4 = FreeCAD.Units.Quantity(reference.Curve.Center[1],FreeCAD.Units.Unit('mm'))  
            z4 = FreeCAD.Units.Quantity(reference.Curve.Center[2],FreeCAD.Units.Unit('mm'))  
        except:    
            x4 = FreeCAD.Units.Quantity(reference.CenterOfMass[0],FreeCAD.Units.Unit('mm'))  
            y4 = FreeCAD.Units.Quantity(reference.CenterOfMass[1],FreeCAD.Units.Unit('mm'))  
            z4 = FreeCAD.Units.Quantity(reference.CenterOfMass[2],FreeCAD.Units.Unit('mm')) 
       
        try:
            x3 = FreeCAD.Units.Quantity(reference1.Curve.Center[0],FreeCAD.Units.Unit('mm'))  
            y3 = FreeCAD.Units.Quantity(reference1.Curve.Center[1],FreeCAD.Units.Unit('mm'))  
            z3 = FreeCAD.Units.Quantity(reference1.Curve.Center[2],FreeCAD.Units.Unit('mm')) 
        except:
            x3 = FreeCAD.Units.Quantity(reference1.CenterOfMass[0],FreeCAD.Units.Unit('mm'))  
            y3 = FreeCAD.Units.Quantity(reference1.CenterOfMass[1],FreeCAD.Units.Unit('mm'))  
            z3 = FreeCAD.Units.Quantity(reference1.CenterOfMass[2],FreeCAD.Units.Unit('mm')) 

        x = (x3+x4)/2
        y = (y3+y4)/2
        z = (z3+z4)/2
     
        obj.addExtension("App::GroupExtensionPython", self)  
        
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","in-line joint","label",1).label = label
        obj.addProperty("App::PropertyString","joint","in-line joint","joint",1).joint = 'in line'
        obj.addProperty("App::PropertyString","node 1","in-line joint","node 1",1).node_1 = node1.label
        obj.addProperty("App::PropertyString","node 2","in-line joint","node 2",1).node_2 = node2.label
        
        #The absolute position is set at the node 1 position, only for animation, not for MBDyn sumulation:        
        obj.addProperty("App::PropertyString","absolute position X","absolute position","absolute position X",1).absolute_position_X = str(round(x.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Y","absolute position","absolute position Y",1).absolute_position_Y = str(round(y.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Z","absolute position","absolute position Z",1).absolute_position_Z = str(round(z.getValueAs('m').Value,precission))+" m" 

        #The relative line position is initially set to (0,0,0), this is, the line passes through node 1
        obj.addProperty("App::PropertyString","relative line position X","line position relative to node 1","relative line position X",1).relative_line_position_X = '0 m'
        obj.addProperty("App::PropertyString","relative line position Y","line position relative to node 1","relative line position Y",1).relative_line_position_Y = '0 m'
        obj.addProperty("App::PropertyString","relative line position Z","line position relative to node 1","relative line position Z",1).relative_line_position_Z = '0 m'
        
        #The relative line orientation is initially set to 3, 0.0, 0.0, 1.0, 2, guess, this is, the line goes paralel to the global Z axis.         
        obj.addProperty("App::PropertyString","relative orientation","relative line orientation","relative orientation",1).relative_orientation = '3, 0.0, 0.0, 1.0, 2, guess'        
        
        #The relative offset is initially set to (0,0,0), this is, node 2 is at the line, without offset
        obj.addProperty("App::PropertyString","relative offset X","line offset relative to node 2","relative offset X",1).relative_offset_X = '0 m'
        obj.addProperty("App::PropertyString","relative offset Y","line offset relative to node 2","relative offset Y",1).relative_offset_Y = '0 m'
        obj.addProperty("App::PropertyString","relative offset Z","line offset relative to node 2","relative offset Z",1).relative_offset_Z = '0 m'
        
        
        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","animation","frame")
        obj.frame=['global','local']  
        
        obj.addProperty("App::PropertyString","structural dummy","animation","structural dummy").structural_dummy = '2'
        
        obj.addProperty("App::PropertyString","force vector multiplier","animation","force vector multiplier").force_vector_multiplier = '1'

        obj.Proxy = self
        
        
        p1 = FreeCAD.Vector(x4, y4, z4)
        #Add z vector of the coordinate system:
        p2 = FreeCAD.Vector(x3, y3, z3)
        l = Draft.makeLine(p1, p2)
        l.Label = 'z: joint: '+ label
        #l.ViewObject.ArrowType = u"Dot"            
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)
        #l.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        l.ViewObject.DrawStyle = u"Dashed"         
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00          
        
        #Add the vector to visualize reaction forces        
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        p1 = FreeCAD.Vector(x, y, z)
        p2 = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)         
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        #d.ViewObject.Selectable = False
        d.Label = "jf: "+ label                                          
        
    def execute(self, fp):
        '''Do something when doing a recomputation, this method is mandatory'''
        ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+fp.label)[0]#get the joint´s line
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)#Max number of decimal places
        #Two 3D points that define the joint´s line:
        p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2]) 
        #A line that defines the joint:
        l1 = Line3D(p1, p2)#Line that defines the joint
        #generate the orientation matrix:   
        fp.relative_orientation = "3, "+ str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) +", "+ str(round(l1.direction[2],precission)) + ", " +"2, guess" #"3, "+ str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) + ", " + str(round(l1.direction[2],precission)) + ", " +"2, guess"                               
        #Get the joint´s nodes:
        node1 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_1)[0]
        node2 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_2)[0]
        
        p3 = Point3D(node2.absolute_position_X.Value, node2.absolute_position_Y.Value, node2.absolute_position_Z.Value)
        
        if (node1.absolute_position_X.Value == node2.absolute_position_X.Value and node1.absolute_position_Y.Value == node2.absolute_position_Y.Value and node1.absolute_position_Z.Value == node2.absolute_position_Z.Value) or (p3 in l1):
            
            fp.relative_offset_X = "0.0 m"
            fp.relative_offset_Y = "0.0 m"
            fp.relative_offset_Z = "0.0 m"
            
        else:    
        
            #Get the position of the nodes:
            p_node1 = Point3D(node1.absolute_position_X.Value, node1.absolute_position_Y.Value, node1.absolute_position_Z.Value)    
            p_node2 = Point3D(node2.absolute_position_X.Value, node2.absolute_position_Y.Value, node2.absolute_position_Z.Value)
            #Calculate a perpendicular line to obtain the line offset relative to node2:
            l2 = l1.perpendicular_line(p_node2)
            #Calculate a perpendicular line to obtain the line position relative to node1:
            l3 = l1.perpendicular_line(p_node1)
            #Update the relative offset:
            relative_offset =  FreeCAD.Vector(float(l2.p2[0]),float(l2.p2[1]),float(l2.p2[2])) - FreeCAD.Vector(float(l2.p1[0]),float(l2.p1[1]),float(l2.p1[2]))         
            fp.relative_offset_X = str(round(relative_offset[0]/1000.0,precission))+" m"
            fp.relative_offset_Y = str(round(relative_offset[1]/1000.0,precission))+" m"
            fp.relative_offset_Z = str(round(relative_offset[2]/1000.0,precission))+" m"
            #Update the relative line position:
            
            if float(l1.distance(p_node1))!=0:                
                relative_line_position =  FreeCAD.Vector(float(l3.p2[0]),float(l3.p2[1]),float(l3.p2[2])) - FreeCAD.Vector(float(l3.p1[0]),float(l3.p1[1]),float(l3.p1[2]))         
                fp.relative_line_position_X = str(relative_line_position[0]/1000.0)+" m"
                fp.relative_line_position_Y = str(relative_line_position[1]/1000.0)+" m"
                fp.relative_line_position_Z = str(relative_line_position[2]/1000.0)+" m"
        
        FreeCAD.Console.PrintMessage("IN-LINE JOINT: " +fp.label+" successful recomputation...\n")                                  
                                  
                                        