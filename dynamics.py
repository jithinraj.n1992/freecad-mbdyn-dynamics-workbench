# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################


#from FreeCAD.Units.Units import FreeCAD.Units.Unit,FreeCAD.FreeCAD.Units.Units.Quantity
import FreeCAD, FreeCADGui
from PySide import QtGui
from random import random
import subprocess
import os
from sys import platform
#Import the required clases
from gravity import Gravity
from mbdyn import MBDyn
from createworld import Createworld
from animation_parameters import Animation_parameters
from rigidbody import Rigidbody
from gear import Gear
from structuraldynamicnode import Structuraldynamicnode
from revolutepin import Revolutepin
from dummynode import Dummynode
from dummybody import Dummybody
from revolutehinge import Revolutehinge
from structuralstaticnode import Structuralstaticnode
from clamp import Clamp
from inline import Inline
from inplane import Inplane
from prismatic import Prismatic
from axialrotation import Axialrotation
from writeinpfile import Writeinpfile
from animation import Animation
from plot import Plotnode, Plotjoint, PlotDrives
from plotscalar import PlotScalars
from customview import CustomView
from customviews import DynamicNodeCustomView, DummyNodeCustomView, StaticNodeCustomView, AbstractNodeCustomView
from structuralforce import StructuralForce
from structuralcouple import StructuralCouple
from deformabledisplacement import DeformableDisplacement
from drivehinge import Drivehinge
from revoluterotation import Revoluterotation
from sphericalhinge import Sphericalhinge
from controldata import ControlData
from scalarfunction import ScalarFunction
from drive import Drive
from totaljoint import Totaljoint
from constitutivelaw import ConstitutiveLaw
from beam3 import Beam3
from viscousbody import ViscousBody
from abstractnode import Abstractnode
from genelclamp import Genelclamp
from plotabstract import Plotabstract

import ObjectsFem
import tempfile

#Import dialogs:
from pluginvariabledialog import PluginVariableDialogAxialRotation, PluginVariableDialogClamp, PluginVariableDialogNode, PluginVariableDialogDynamicNode

ani = Animation()

__dir__ = os.path.dirname(__file__)

class Dynamics:
    def __init__(self):
        return

#########################################################################################################################################################################################################################################################    
    def AddPlugin(self, objeto):   
        if objeto.Label.startswith('joint: '):
            if objeto.joint == "axial rotation" or objeto.joint == "revolute pin" or objeto.joint == "revolute hinge":
                panel = PluginVariableDialogAxialRotation(objeto)
                FreeCADGui.Control.showDialog(panel)

            if objeto.joint == "clamp":
                panel = PluginVariableDialogClamp(objeto)
                FreeCADGui.Control.showDialog(panel)

        if objeto.Label.startswith('structural: '):
            if objeto.type == "static" or objeto.type == "dummy":
                panel = PluginVariableDialogNode(objeto)
                FreeCADGui.Control.showDialog(panel)

            if objeto.type == "dynamic":
                panel = PluginVariableDialogDynamicNode(objeto)
                FreeCADGui.Control.showDialog(panel)   
                
#########################################################################################################################################################################################################################################################    
    def Hide(self, jo):
        for obj in FreeCAD.ActiveDocument.Objects:
            if obj.Label.startswith('structural: ') or obj.Label.startswith('joint: ') or obj.Label.startswith('body: ') or obj.Label.startswith('force: '): 
                FreeCADGui.ActiveDocument.getObject(obj.Name).Visibility = False
                FreeCAD.ActiveDocument.getObject(obj.Name).purgeTouched()

        if jo.joint=="revolute pin":            
            for obj in FreeCAD.ActiveDocument.Objects:
                if obj.Label == 'body: '+jo.node or obj.Label == 'joint: '+jo.label: 
                    FreeCADGui.ActiveDocument.getObject(obj.Name).Visibility = True 
                    FreeCAD.ActiveDocument.getObject(obj.Name).purgeTouched()

        if jo.joint=="clamp":            
            for obj in FreeCAD.ActiveDocument.Objects:
                if obj.Label == 'body: '+jo.node_label or obj.Label == 'joint: '+jo.label: 
                    FreeCADGui.ActiveDocument.getObject(obj.Name).Visibility = True 
                    FreeCAD.ActiveDocument.getObject(obj.Name).purgeTouched()
                    
        if jo.joint=="revolute hinge" or jo.joint=="in line" or jo.joint=="drive hinge" or jo.joint=="prismatic" or jo.joint=="spherical hinge" or jo.joint=="deformable displacement joint: damper" or jo.joint=="deformable displacement joint: spring" or jo.joint=="total joint" or jo.joint=="revolute rotation" or jo.joint=="deformable displacement":            
            for obj in FreeCAD.ActiveDocument.Objects:
                if obj.Label == 'structural: '+jo.node_1 or obj.Label == 'structural: '+jo.node_2 or obj.Label == 'joint: '+jo.label or obj.Label == 'body: '+jo.node_1 or obj.Label == 'body: '+jo.node_2: 
                    FreeCADGui.ActiveDocument.getObject(obj.Name).Visibility = True 
                    FreeCAD.ActiveDocument.getObject(obj.Name).purgeTouched()
                    
        if jo.joint=="axial rotation":          
            for obj in FreeCAD.ActiveDocument.Objects:
                if obj.Label == 'structural: '+jo.node_1 or obj.Label == 'structural: '+jo.node_2 or obj.Label == 'joint: '+jo.label or obj.Label == 'body: '+jo.node_1 or obj.Label == 'body: '+jo.node_2: 
                    FreeCADGui.ActiveDocument.getObject(obj.Name).Visibility = True 
                    FreeCAD.ActiveDocument.getObject(obj.Name).purgeTouched()
#########################################################################################################################################################################################################################################################    
    def View(self):    
        for obj in FreeCAD.ActiveDocument.Objects:
            if obj.Label.startswith('structural: ') or obj.Label.startswith('joint: ') or obj.Label.startswith('body: '): 
                FreeCADGui.ActiveDocument.getObject(obj.Name).Visibility = True
                FreeCAD.ActiveDocument.getObject(obj.Name).purgeTouched()

#########################################################################################################################################################################################################################################################    
    def Redirection(self, objs):          
        startpoint = objs.Start 
        endpoint = objs.End
        objs.Start = startpoint-startpoint
        objs.End = -1*(endpoint-startpoint)
        objs.Start = startpoint
        objs.End = objs.End +startpoint
        FreeCAD.ActiveDocument.recompute() 
                
#########################################################################################################################################################################################################################################################    
    def RelocateZ(self, objs):        
        try:
            objs[0].absolute_position_Z = FreeCAD.Units.Quantity(objs[1].Shape.Solids[0].CenterOfMass[2],FreeCAD.Units.Unit('mm'))
        except:
            try:
                objs[0].absolute_position_Z = objs[1].position_Z 
            except:
                try:
                    objs[0].absolute_position_Z = objs[1].Placement.Base[2]
                except:
                    try:
                        objs[0].End = (objs[0].Start[0], objs[0].Start[1], objs[0].Start[2] + objs[0].Length.Value)
                    except:
                        try:
                            objs.End = (objs.Start[0], objs.Start[1], objs.Start[2] + objs.Length.Value)
                        except:
                            pass
        FreeCAD.ActiveDocument.recompute() 
                    
#########################################################################################################################################################################################################################################################    
    def RelocateY(self, objs):
        try:
            objs[0].absolute_position_Y = FreeCAD.Units.Quantity(objs[1].Shape.Solids[0].CenterOfMass[1],FreeCAD.Units.Unit('mm'))
        except:
            try:
                objs[0].absolute_position_Y = objs[1].position_Y
            except:
                try:
                    objs[0].absolute_position_Y = objs[1].Placement.Base[1]
                except:
                    try:
                        objs[0].End = (objs[0].Start[0], objs[0].Start[1] + objs[0].Length.Value, objs[0].Start[2]) 
                    except:
                        try:
                            objs.End = (objs.Start[0], objs.Start[1] + objs.Length.Value, objs.Start[2]) 
                        except:
                            pass 
        FreeCAD.ActiveDocument.recompute() 
                    
#########################################################################################################################################################################################################################################################    
    def RelocateX(self, objs):
        try:
            objs[0].absolute_position_X = FreeCAD.Units.Quantity(objs[1].Shape.Solids[0].CenterOfMass[0],FreeCAD.Units.Unit('mm'))
        except:
            try:
                objs[0].absolute_position_X = objs[1].position_X
            except:
                try:
                    objs[0].absolute_position_X = objs[1].Placement.Base[0]
                except:
                    try:
                        objs[0].End = (objs[0].Start[0] + objs[0].Length.Value, objs[0].Start[1], objs[0].Start[2]) 
                    except:
                        try:
                            objs.End = (objs.Start[0] + objs.Length.Value, objs.Start[1], objs.Start[2])
                        except:
                            pass
        FreeCAD.ActiveDocument.recompute() 
                    
#########################################################################################################################################################################################################################################################    
    def Relocate(self):
        X, Y, Z = 0, 0, 0 
        mX, mY, mZ = 0, 0, 0 
        if len(FreeCADGui.Selection.getSelectionEx()[1].SubObjects) == 1:
            try:
                X = FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].Curve.Center[0]
                Y = FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].Curve.Center[1]
                Z = FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].Curve.Center[2] 
            except:
                try:
                    X = FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].Point[0]
                    Y = FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].Point[1]
                    Z = FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].Point[2]  
                except:
                    X = FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].CenterOfMass[0]
                    Y = FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].CenterOfMass[1]
                    Z = FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].CenterOfMass[2]
        
                                    
        if len(FreeCADGui.Selection.getSelectionEx()[1].SubObjects) == 0:
            try:
                for obj in range(1,len(FreeCADGui.Selection.getSelection())):
                    X = X + (FreeCADGui.Selection.getSelection()[obj].Shape.Solids[0].CenterOfMass[0] * float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0]))
                    Y = Y + (FreeCADGui.Selection.getSelection()[obj].Shape.Solids[0].CenterOfMass[1] * float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0]))
                    Z = Z + (FreeCADGui.Selection.getSelection()[obj].Shape.Solids[0].CenterOfMass[2] * float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0]))

                    mX = mX + float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0])
                    mY = mY + float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0])
                    mZ = mZ + float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0])
                    
                X = X / mX
                Y = Y / mY
                Z = Z / mZ
                
            except:
                try:
                    X = FreeCADGui.Selection.getSelection()[1].absolute_position_X
                    Y = FreeCADGui.Selection.getSelection()[1].absolute_position_Y
                    Z = FreeCADGui.Selection.getSelection()[1].absolute_position_Z
                except:
                    try:
                        X = FreeCADGui.Selection.getSelection()[1].Placement.Base[0]
                        Y = FreeCADGui.Selection.getSelection()[1].Placement.Base[1]
                        Z = FreeCADGui.Selection.getSelection()[1].Placement.Base[2]
                    except:
                        pass

        if FreeCADGui.Selection.getSelection()[0].Label.startswith('structural: '):
            FreeCADGui.Selection.getSelection()[0].absolute_position_X = X
            FreeCADGui.Selection.getSelection()[0].absolute_position_Y = Y
            FreeCADGui.Selection.getSelection()[0].absolute_position_Z = Z
                        
        if FreeCADGui.Selection.getSelection()[0].Label.startswith('z: joint:') or FreeCADGui.Selection.getSelection()[0].Label.startswith('z: gravity:') or FreeCADGui.Selection.getSelection()[0].Label.startswith('z: force:') or FreeCADGui.Selection.getSelection()[0].Label.startswith('z: couple:'):   
            FreeCADGui.Selection.getSelection()[0].End = (X,Y,Z)
            #FreeCADGui.Selection.getSelection()[0].Start = (X1,Y1,Z1) 
              
        FreeCAD.ActiveDocument.recompute() 
        
#########################################################################################################################################################################################################################################################    
    def AddStructuralForce(self):
        node = FreeCADGui.Selection.getSelection()[0]                   
        existingforces = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('force') or obj1.Label.startswith('couple'):
                existingforces = existingforces+1
                
        obj = 'force_'+node.label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        StructuralForce(a,str(existingforces),node)
        CustomView(a.ViewObject)
        #Move the object to it's container            
        FreeCAD.ActiveDocument.getObject("Structural_forces").addObject(a)
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("z: force: "+a.label)[0]) 
        a.Label='force: '+str(existingforces) #rename the joint 
        FreeCAD.ActiveDocument.recompute()

#########################################################################################################################################################################################################################################################    
    def AddDrive(self):
        existingdrives = 1
        for obj in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj.Label.startswith('drive'):
                existingdrives = existingdrives + 1
                
        obj = 'function_'+str(existingdrives)
        a = FreeCAD.ActiveDocument.addObject("App::FeaturePython",obj)
        Drive(a, str(existingdrives))
        CustomView(a.ViewObject)
        #Move the object to it's container            
        FreeCAD.ActiveDocument.getObject("Drives").addObject(a)       
        a.Label='drive: '+str(existingdrives)
        FreeCAD.ActiveDocument.recompute()
        return a

#########################################################################################################################################################################################################################################################    
    def AddScalarFunction(self):
        if len(FreeCADGui.Selection.getSelectionEx()) == 0:
                reply = QtGui.QInputDialog.getText(None,"Dynamics","Enter the number of steps (an integer):")
                if reply[1]:
                    npoints = int(reply[0])
                    Yvalues = "None"
                    Xvalues = "None"
                    
        else:
            reference = FreeCADGui.Selection.getSelection()[0] 
            
            reply = QtGui.QInputDialog.getText(None,"Dynamics","""To create a polar function type "polar".
To create a Cartesian function type "cartesian".""")

            if reply[1]:
                ftype = reply[0]
                if ftype.upper()=="POLAR":
                    veccenter = FreeCAD.Vector(reference.absolute_position_X,reference.absolute_position_Y,reference.absolute_position_Z)
                    
                if ftype.upper()=="CARTESIAN":
                    reply = QtGui.QInputDialog.getText(None,"Dynamics","""Select axis (X, Y, or Z).""")
                    axis = reply[0]
                    if axis.upper() == "X":
                        veccenter = reference.absolute_position_X.Value

                    if axis.upper() == "Y":
                        veccenter = reference.absolute_position_Y.Value
                        
                    if axis.upper() == "Z":
                        veccenter = reference.absolute_position_Z.Value
                        
            reply = QtGui.QInputDialog.getText(None,"Dynamics","""Enter number of points per segment. Eg: "20,30,40,90,...".
If only one number is entered (Eg. "100"), this number will be applied to all the segments.
In the case of polar functions, these numbers must be proportional to the angle corresponding to each segment.
In the case of Cartesian functions, these numbers must be proportional to the length corresponding to each segment.""")
            if reply[1]:
                steps = reply[0]                    
                
                points = FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0].discretize(Number = int(steps.split(",")[0]))
                
                reply = QtGui.QInputDialog.getText(None,"Dynamics",'Invert direction?. Type "yes" or "no".')
                if reply[1]:
                    invert = reply[0]
                    if invert=="yes":
                        points = points[::-1]
                
                lines = FreeCADGui.Selection.getSelectionEx()[1].SubObjects[1:]
                countpoints = 1
                
                for obj in lines:
                    if len(steps.split(","))==1:
                        aux = obj.discretize(Number = int(steps)) 
                    else:
                        aux = obj.discretize(Number = int(steps.split(",")[countpoints])) 
                        countpoints = countpoints + 1
                        
                    if points[-1].sub(aux[0]).Length > points[-1].sub(aux[-1]).Length:
                        aux = aux[::-1]
                            
                    for obj1 in aux:
                        points.append (obj1)

                                                                                                         
                Yvalues = []
                if ftype=="polar":
                    for n in points:
                        Yvalues.append(n.sub(veccenter).Length/1000.)                    

                if ftype=="cartesian":
                    if axis.upper() == "X":
                        for n in points:
                            Yvalues.append((n[0]-veccenter)/1000.)

                    if axis.upper() == "Y":
                        for n in points:
                            Yvalues.append((n[1]-veccenter)/1000.)
                        
                    if axis.upper() == "Z":
                        for n in points:
                            Yvalues.append((n[2]-veccenter)/1000.)
 
                        
                Xvalues = "None"
                npoints = len(Yvalues)  

        #create the scalar function:        
        existingfunctions = 1
        for obj in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj.Label.startswith('function'):
                existingfunctions = existingfunctions+1
                
        obj = 'function_'+str(existingfunctions)
        a = FreeCAD.ActiveDocument.addObject("App::FeaturePython",obj)
        ScalarFunction(a, str(existingfunctions), npoints, Yvalues, Xvalues)
        CustomView(a.ViewObject)
        a.Label='function: '+str(existingfunctions)
        #Move the object to it's container            
        FreeCAD.ActiveDocument.getObject("Scalar_functions").addObject(a)
        FreeCAD.ActiveDocument.getObjectsByLabel("function: "+str(existingfunctions))[0].addObject(FreeCAD.ActiveDocument.getObject("points_"+str(existingfunctions)))        
        FreeCAD.ActiveDocument.recompute()
        
#########################################################################################################################################################################################################################################################    
    def AddStructuralCouple(self):
        node = FreeCADGui.Selection.getSelection()[0]
        existingforces = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('force') or obj1.Label.startswith('couple'):
                existingforces = existingforces+1
                
        obj = 'couple_'+node.label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        StructuralCouple(a,str(existingforces),node)
        CustomView(a.ViewObject)
        #Move the object to it's container            
        FreeCAD.ActiveDocument.getObject("Structural_couples").addObject(a)
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("z: couple: "+a.label)[0]) 
        a.Label='couple: '+str(existingforces) #rename the joint 
        FreeCAD.ActiveDocument.recompute()

#########################################################################################################################################################################################################################################################    
    def AddRigidBody(self, baseBody):#This metod receives a non-parametric body and creates a rigid body
        existingbodies = 0
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing bodies            
            if obj1.Label.startswith('body'):
                existingbodies = existingbodies+1
        #If a body with the next label already exists, rename it:
        if(len(FreeCAD.ActiveDocument.getObjectsByLabel(str(existingbodies+1)))>0):
            FreeCAD.ActiveDocument.getObjectsByLabel(str(existingbodies+1))[0].Label=str(existingbodies+2)
                        
        baseBody.Label=str(existingbodies+1)                
        FreeCADGui.ActiveDocument.getObject(baseBody.Name).Visibility=False
        obj = 'body_'+baseBody.Label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Rigidbody(a,baseBody)#Create the rigid body
        a.Shape = baseBody.Shape 
        a.Label='body: '+baseBody.Label
        CustomView(a.ViewObject)
        FreeCAD.ActiveDocument.getObject("Generic").addObject(a)              
        a.ViewObject.Transparency = 85
        a.ViewObject.LineWidth = 2.00
        a.ViewObject.PointSize = 3.00
        c1,c2,c3=random(),random(),random()
        a.ViewObject.LineColor = (c1,c2,c3)
        a.ViewObject.PointColor = (c1,c2,c3)
        a.ViewObject.ShapeColor = (c1,c2,c3)
        #a.ViewObject.Selectable = False
        a.ViewObject.Proxy=0 
        #Add the material to the rigid body:
        material_object = ObjectsFem.makeMaterialSolid(FreeCAD.ActiveDocument)
        material_object.Label="material: "+baseBody.Label        
        mat = material_object.Material
        mat['Name'] = "Steel-Generic"
        mat['Density'] = "7900.00 kg/m^3"
        material_object.Material = mat
        a.addObject(material_object) 
        FreeCAD.ActiveDocument.recompute() 
        return a
        
#########################################################################################################################################################################################################################################################    
    def AddGear(self, baseBody):#This metod receives a non-parametric body and creates a rigid body
        existingbodies = 0
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing bodies            
            if obj1.Label.startswith('body'):
                existingbodies = existingbodies+1
        #If a body with the next label already exists, rename it:
        if(len(FreeCAD.ActiveDocument.getObjectsByLabel(str(existingbodies+1)))>0):
            FreeCAD.ActiveDocument.getObjectsByLabel(str(existingbodies+1))[0].Label=str(existingbodies+2)
                        
        baseBody.Label=str(existingbodies+1)                
        FreeCADGui.ActiveDocument.getObject(baseBody.Name).Visibility=False
        obj = 'body_'+baseBody.Label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Gear(a,baseBody)#Create the rigid body
        a.Shape = baseBody.Shape 
        a.Label='body: '+baseBody.Label
        CustomView(a.ViewObject)
        FreeCAD.ActiveDocument.getObject("Gears").addObject(a)              
        a.ViewObject.Transparency = 85
        a.ViewObject.LineWidth = 1.00
        a.ViewObject.PointSize = 1.00
        c1,c2,c3=random(),random(),random()
        a.ViewObject.LineColor = (c1,c2,c3)
        a.ViewObject.PointColor = (c1,c2,c3)
        a.ViewObject.ShapeColor = (c1,c2,c3)
        #a.ViewObject.Selectable = False
        a.ViewObject.Proxy=0 
        #Add the material to the rigid body:
        material_object = ObjectsFem.makeMaterialSolid(FreeCAD.ActiveDocument)
        material_object.Label="material: "+baseBody.Label        
        mat = material_object.Material
        mat['Name'] = "Steel-Generic"
        mat['Density'] = "7900.00 kg/m^3"
        material_object.Material = mat
        a.addObject(material_object) 
                        
#########################################################################################################################################################################################################################################################            
    def AddDummyBody(self, baseBody):
        FreeCADGui.ActiveDocument.getObject(baseBody.Name).Visibility=False
        existingbodies = 0
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing bodies            
            if obj1.Label.startswith('body'):
                existingbodies = existingbodies+1
        #If a adding body withe the label already exists, rename it:
        if(len(FreeCAD.ActiveDocument.getObjectsByLabel(str(existingbodies+1)))>0):
            FreeCAD.ActiveDocument.getObjectsByLabel(str(existingbodies+1))[0].Label=str(existingbodies+2)        

        baseBody.Label=str(existingbodies+1)        
        obj = 'body_'+baseBody.Label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Dummybody(a, baseBody, baseBody.Label)
        CustomView(a.ViewObject)
        FreeCAD.ActiveDocument.getObject("Dummy_bodies").addObject(FreeCAD.ActiveDocument.getObjectsByLabel(obj)[0])
        FreeCAD.ActiveDocument.getObjectsByLabel(obj)[0].Shape = baseBody.Shape
        FreeCAD.ActiveDocument.getObjectsByLabel(obj)[0].Label='body: '+baseBody.Label
        FreeCADGui.ActiveDocument.getObject(obj).Transparency = 85
        FreeCADGui.ActiveDocument.getObject(obj).LineWidth = 1.00
        FreeCADGui.ActiveDocument.getObject(obj).PointSize = 1.00
        a.ViewObject.Proxy=0   
        FreeCAD.ActiveDocument.recompute()
        
#########################################################################################################################################################################################################################################################            
    def AddStaticBody(self, baseBody):
        FreeCADGui.ActiveDocument.getObject(baseBody.Name).Visibility=False
        existingbodies = 0
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing bodies            
            if obj1.Label.startswith('body'):
                existingbodies = existingbodies+1
        #If a adding body withe the label already exists, rename it:
        if(len(FreeCAD.ActiveDocument.getObjectsByLabel(str(existingbodies+1)))>0):
            FreeCAD.ActiveDocument.getObjectsByLabel(str(existingbodies+1))[0].Label=str(existingbodies+2)        

        baseBody.Label=str(existingbodies+1)        
        obj = 'body_'+baseBody.Label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Dummybody(a, baseBody, baseBody.Label)
        CustomView(a.ViewObject)
        FreeCAD.ActiveDocument.getObject("Static_bodies").addObject(FreeCAD.ActiveDocument.getObjectsByLabel(obj)[0])
        FreeCAD.ActiveDocument.getObjectsByLabel(obj)[0].Shape = baseBody.Shape
        FreeCAD.ActiveDocument.getObjectsByLabel(obj)[0].Label='body: '+baseBody.Label
        FreeCADGui.ActiveDocument.getObject(obj).Transparency = 85
        FreeCADGui.ActiveDocument.getObject(obj).LineWidth = 2.00
        FreeCADGui.ActiveDocument.getObject(obj).PointSize = 2.00
        #FreeCADGui.ActiveDocument.getObject(obj).Selectable = False
        a.ViewObject.Proxy=0    
        
#########################################################################################################################################################################################################################################################        
    def AddStructuralDynamicNode(self, baseBody):
        obj = 'structural_'+baseBody.Label
        node = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Structuraldynamicnode(node,baseBody)
        #CustomView(node.ViewObject)
        DynamicNodeCustomView(node.ViewObject)
        node.Label = 'structural: '+baseBody.Label  
        #Move objects to their container
        FreeCAD.ActiveDocument.getObjectsByLabel('body: '+baseBody.Label)[0].addObject(node)
        node.addObject(FreeCAD.ActiveDocument.getObjectsByLabel('x: structural: '+baseBody.Label)[0])
        node.addObject(FreeCAD.ActiveDocument.getObjectsByLabel('y: structural: '+baseBody.Label)[0])
        node.addObject(FreeCAD.ActiveDocument.getObjectsByLabel('z: structural: '+baseBody.Label)[0])   
        FreeCAD.ActiveDocument.recompute()
        return node
        
#########################################################################################################################################################################################################################################################               
    def AddStructuralStaticNode(self, baseBody):    
        obj = 'structural_'+baseBody.Label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Structuralstaticnode(a,baseBody)
        StaticNodeCustomView(a.ViewObject)
        a.Label = "structural: "+baseBody.Label
        #Move objects to their container:
        FreeCAD.ActiveDocument.getObjectsByLabel("body: "+baseBody.Label)[0].addObject(a) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("x: structural: "+baseBody.Label)[0])
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("y: structural: "+baseBody.Label)[0])
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("z: structural: "+baseBody.Label)[0])  
        FreeCAD.ActiveDocument.recompute()
        
#########################################################################################################################################################################################################################################################               
    def AddStructuralDumyNode(self, node, baseBody):
        obj = 'structural_'+baseBody.Label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Dummynode(a, node ,baseBody) 
        DummyNodeCustomView(a.ViewObject)
        a.Label='structural: '+baseBody.Label 
        #Move objects to their container
        FreeCAD.ActiveDocument.getObjectsByLabel("body: "+baseBody.Label)[0].addObject(a)
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("x: structural: "+baseBody.Label)[0])
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("y: structural: "+baseBody.Label)[0])
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("z: structural: "+baseBody.Label)[0])
        FreeCAD.ActiveDocument.recompute()

#########################################################################################################################################################################################################################################################               
    def AddAbstractNode(self):
        existingabstracts = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing abstract nodes          
            if obj1.Label.startswith('abstract'):
                existingabstracts = existingabstracts+1
        obj = 'abstract_'+str(existingabstracts)
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)        
        Abstractnode(a, str(existingabstracts)) 
        AbstractNodeCustomView(a.ViewObject)
        a.Label='abstract: '+str(existingabstracts)
        #Move objects to their container
        FreeCAD.ActiveDocument.getObjectsByLabel("Abstract_nodes")[0].addObject(a)
        FreeCAD.ActiveDocument.recompute()
        
#########################################################################################################################################################################################################################################################               
    def AddClampJoint(self):
        node = FreeCADGui.Selection.getSelection()[0]
        #Count the existing joints:             
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        obj = 'joint_'+node.label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Clamp(a,str(existingjoints),node) 
        CustomView(a.ViewObject)   
        a.ViewObject.Transparency = 85
        a.ViewObject.LineWidth = 1.00  
        #Move objects to their containers:        
        FreeCAD.ActiveDocument.getObject('Clamp_joints').addObject(a) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0])            
        a.Label='joint: '+str(existingjoints) #rename the clamp
        FreeCAD.ActiveDocument.recompute()  
        
#########################################################################################################################################################################################################################################################               
    def AddGenelClamp(self, node):
        existingenels = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing abstract nodes          
            if obj1.Label.startswith('genel:'):
                existingenels = existingenels+1
        obj = 'genel_'+str(existingenels)
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)        
        Genelclamp(a, str(existingenels), node)
        CustomView(a.ViewObject)
        a.Label='genel: '+str(existingenels)
        #Move objects to their container
        FreeCAD.ActiveDocument.getObjectsByLabel("Genel_clamps")[0].addObject(a)
        FreeCAD.ActiveDocument.recompute()
        
#########################################################################################################################################################################################################################################################                               
    def AddRevoluteRotation(self):#This method receives a dynamic node and a cylinder and adds a revolute pin to the node, and around the center of mass of the cylinder
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
        try:
            ref1 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[0]
            ref2 = FreeCADGui.Selection.getSelectionEx()[3].SubObjects[0]
        except:
            ref1 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[0]
            ref2 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[1]    
    
        obj = 'joint_'+node1.label+'_'+node2.label
        #Count the existing joints:             
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Revoluterotation(a,str(existingjoints), node1, node2, ref1, ref2)
        CustomView(a.ViewObject)   
        #ManageAxes.AddAxes(cylinder,'joint',str(existingjoints))
        a.Label='joint: '+str(existingjoints) #rename the hinge
        #Move objects to their containers:
        FreeCAD.ActiveDocument.getObject('Revolute_rotation_joints').addObject(a) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+str(existingjoints))[0])  
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0])    
        FreeCAD.ActiveDocument.recompute()   
               
#########################################################################################################################################################################################################################################################                               
    def AddRevolutePin(self):
        node = FreeCADGui.Selection.getSelection()[0]
        try:
            ref1 = FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0]
            ref2 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[0]
        except:
            ref1 = FreeCADGui.Selection.getSelectionEx()[1].SubObjects[0]
            ref2 = FreeCADGui.Selection.getSelectionEx()[1].SubObjects[1]
            
        obj = 'joint_'+node.label
        #Count the existing joints:             
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Revolutepin(a,str(existingjoints),node, ref1, ref2)             
        CustomView(a.ViewObject)
        a.ViewObject.Transparency = 85
        a.ViewObject.LineWidth = 1.00    
        a.Label='joint: '+str(existingjoints) #rename the hinge 
        #Move objects to their containers:
        FreeCAD.ActiveDocument.getObject('Revolute_pin_joints').addObject(a)
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+str(existingjoints))[0]) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0])  
        FreeCAD.ActiveDocument.recompute()  
                     
#########################################################################################################################################################################################################################################################                               
    def AddDriveHingeJoint(self):
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
        obj = 'joint_'+node1.label+'_'+node2.label
        #Count the existing joints:             
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Drivehinge(a,str(existingjoints), node1, node2)
        CustomView(a.ViewObject)   
        a.Label='joint: '+str(existingjoints) #rename the hinge
        #Move objects to their containers:
        FreeCAD.ActiveDocument.getObject('Drive_hinge_joints').addObject(a) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+str(existingjoints))[0])  
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0])         

#########################################################################################################################################################################################################################################################                               
    def AddBeam3(self, node1, node2, node3, existingbeams, law = "please type the label of a 6D constitutive law"):        
        obj = 'beam3_'+node1.label+'_'+node2.label+'_'+node3.label
        #Count the existing joints:             
        existingbeam3s = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('beam3'):
                existingbeam3s = existingbeam3s+1
                
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Beam3(a,str(existingbeam3s), node1, node2, node3)
        CustomView(a.ViewObject)  
        a.constitutive_law_section_I = law
        a.constitutive_law_section_II = law
        #ManageAxes.AddAxes(cylinder,'joint',str(existingjoints))
        a.Label='beam3: '+str(existingbeam3s) #rename the hinge
        #Move objects to their containers:
        FreeCAD.ActiveDocument.getObjectsByLabel('Beam: '+existingbeams)[0].addObject(a) 

#########################################################################################################################################################################################################################################################                               
    def AddViscousBody(self, node, law = "please type the label of a 6D constitutive law"):        
        #Count the existing joints:             
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        obj = 'joint_'+node.label+'_'+str(existingjoints)
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        ViscousBody(a, str(existingjoints), node, law) 
        CustomView(a.ViewObject)    
        #Move objects to their containers:        
        FreeCAD.ActiveDocument.getObject('Viscous_body_joints').addObject(a)  
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0])          
        a.Label='joint: '+str(existingjoints) #rename the clamp
        FreeCAD.ActiveDocument.recompute() 
        return a
        
#########################################################################################################################################################################################################################################################                               
    def AddTotalJoint(self):
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
        
        #Count the existing joints:             
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        obj = 'joint_'+node1.label+'_'+node2.label+'_'+str(existingjoints)


                
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Totaljoint(a, str(existingjoints), node1, node2)
        CustomView(a.ViewObject)         
        a.Label='joint: '+str(existingjoints) 
        #Move objects to their containers:
        FreeCAD.ActiveDocument.getObject('Total_joints').addObject(a) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0])    
        FreeCAD.ActiveDocument.recompute()  
        
#########################################################################################################################################################################################################################################################                               
    def AddRevoluteHingeJoint(self):
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
        try:
            ref1 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[0]
            ref2 = FreeCADGui.Selection.getSelectionEx()[3].SubObjects[0]
        except:
            ref1 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[0]
            ref2 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[1]
                        
        obj = 'joint_'+node1.label+'_'+node2.label

        #Count the existing joints:             
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Revolutehinge(a, str(existingjoints), node1, node2, ref1, ref2)
        CustomView(a.ViewObject)  
        a.ViewObject.Transparency = 85
        a.ViewObject.LineWidth = 1.00        
        a.Label='joint: '+str(existingjoints) #rename the hinge
        #Move objects to their containers:
        FreeCAD.ActiveDocument.getObject('Revolute_hinge_joints').addObject(a) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+str(existingjoints))[0])  
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0])    
        FreeCAD.ActiveDocument.recompute()             
        
#########################################################################################################################################################################################################################################################               
    def AddInLineJoint(self):
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
        try:
            ref1 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[0]
            ref2 = FreeCADGui.Selection.getSelectionEx()[3].SubObjects[0]
        except:
            ref1 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[0]
            ref2 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[1]
            
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        obj = 'joint_'+node1.label+'_'+node2.label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Inline(a,str(existingjoints), node1 ,node2, ref1, ref2)
        CustomView(a.ViewObject)
        a.ViewObject.Transparency = 85
        a.ViewObject.LineWidth = 1.00
        #Move the object to it's container            
        FreeCAD.ActiveDocument.getObject("In_line_joints").addObject(a)
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+str(existingjoints))[0]) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0]) 
        a.Label='joint: '+str(existingjoints) #rename the joint    
        FreeCAD.ActiveDocument.recompute()
        
#########################################################################################################################################################################################################################################################               
    def AddInPlaneJoint(self):
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        obj = 'joint_'+node1.label+'_'+node2.label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Inplane(a,str(existingjoints),node1,node2)
        CustomView(a.ViewObject)
        #Move the object to it's container            
        FreeCAD.ActiveDocument.getObject("In_plane_joints").addObject(a)
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+str(existingjoints))[0]) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0]) 
        a.Label='joint: '+str(existingjoints) #rename the joint 
        FreeCAD.ActiveDocument.recompute()
         
#########################################################################################################################################################################################################################################################               
    def AddSphericalHinge(self):
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1] 
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        obj = 'joint_'+node1.label+'_'+node2.label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Sphericalhinge(a,str(existingjoints),node1,node2)
        CustomView(a.ViewObject)
        a.ViewObject.Transparency = 85
        a.ViewObject.LineWidth = 1.00 
        #Move the object to it's container            
        FreeCAD.ActiveDocument.getObject("Spherical_hinge_joints").addObject(a)
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0]) 
        a.Label='joint: '+str(existingjoints) #rename the joint      
        FreeCAD.ActiveDocument.recompute()  
        
#########################################################################################################################################################################################################################################################               
    def AddPrismaticJoint(self):
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        obj = 'joint_'+node1.label+'_'+node2.label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Prismatic(a,str(existingjoints),node2,node1)
        CustomView(a.ViewObject)
        a.ViewObject.Transparency = 85
        a.ViewObject.LineWidth = 1.00
        #Move the object to it's container            
        FreeCAD.ActiveDocument.getObject("Prismatic_joints").addObject(a) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0]) 
        a.Label='joint: '+str(existingjoints) #rename the joint 
        FreeCAD.ActiveDocument.recompute()  
        
#########################################################################################################################################################################################################################################################               
    def AddLockJoint(self):
        existinglocks = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:
            if obj1.Label.startswith('coincidence'):
                existinglocks = existinglocks+1
                
        a = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","coincidence: "+str(existinglocks)) 
        FreeCAD.ActiveDocument.getObject("Coincidence_joints").addObject(a) 
        a.Label='coincidence: '+str(existinglocks)

        node1 = FreeCADGui.Selection.getSelection()[0] 
        node2 = FreeCADGui.Selection.getSelection()[1]
        
        #Add a prismatic joint:
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        obj = 'joint_'+node1.label+'_'+node2.label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Prismatic(a,str(existingjoints),node2,node1)
        CustomView(a.ViewObject)
        a.ViewObject.Transparency = 85
        a.ViewObject.LineWidth = 1.00
        a.Label='joint: '+str(existingjoints) #rename the joint
                
        #Move the object to it's container            
        FreeCAD.ActiveDocument.getObjectsByLabel("coincidence: "+str(existinglocks))[0].addObject(a) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0]) 
   
        #Add a spherical joint:
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        obj = 'joint_'+node1.label+'_'+node2.label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Sphericalhinge(a,str(existingjoints),node1,node2)
        CustomView(a.ViewObject)
        a.ViewObject.Transparency = 85
        a.ViewObject.LineWidth = 1.00 
        a.Label='joint: '+str(existingjoints) #rename the joint        
        
        #Move the object to it's container            
        FreeCAD.ActiveDocument.getObjectsByLabel("coincidence: "+str(existinglocks))[0].addObject(a)
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0])         
        FreeCAD.ActiveDocument.recompute()  

#########################################################################################################################################################################################################################################################               
    def AddCyindricalJoint(self):
        existingcylindricals = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:
            if obj1.Label.startswith('cylindrical'):
                existingcylindricals = existingcylindricals+1
                
        a = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","cylindrical: "+str(existingcylindricals)) 
        FreeCAD.ActiveDocument.getObject("Cylindrical_joints").addObject(a) 
        a.Label='cylindrical: '+str(existingcylindricals)

        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
        
        try:
            ref1 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[0]
            ref2 = FreeCADGui.Selection.getSelectionEx()[3].SubObjects[0]
        except:
            ref1 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[0]
            ref2 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[1]
            
        #in-line joint:
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        obj = 'joint_'+node1.label+'_'+node2.label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Inline(a,str(existingjoints), node1 ,node2, ref1, ref2)
        CustomView(a.ViewObject)
        a.ViewObject.Transparency = 85
        a.ViewObject.LineWidth = 1.00
        a.Label='joint: '+str(existingjoints)
                
        #Move the object to it's container            
        FreeCAD.ActiveDocument.getObjectsByLabel("cylindrical: "+str(existingcylindricals))[0].addObject(a) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+str(existingjoints))[0]) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0]) 
   
        #revolute rotation joint:
        obj = 'joint_'+node1.label+'_'+node2.label
        #Count the existing joints:             
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Revoluterotation(a,str(existingjoints), node1, node2, ref1, ref2)
        CustomView(a.ViewObject)   
        #ManageAxes.AddAxes(cylinder,'joint',str(existingjoints))
        a.Label='joint: '+str(existingjoints) #rename the hinge
        
        #Move the object to it's container            
        FreeCAD.ActiveDocument.getObjectsByLabel("cylindrical: "+str(existingcylindricals))[0].addObject(a) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+str(existingjoints))[0]) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0]) 
         
        FreeCAD.ActiveDocument.recompute()  

#########################################################################################################################################################################################################################################################               
    def AddSliderJoint(self):
        existingsliders = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:
            if obj1.Label.startswith('slider'):
                existingsliders = existingsliders+1
                
        a = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","slider: "+str(existingsliders)) 
        FreeCAD.ActiveDocument.getObject("Slider_joints").addObject(a) 
        a.Label='slider: '+str(existingsliders)

        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
        try:
            ref1 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[0]
            ref2 = FreeCADGui.Selection.getSelectionEx()[3].SubObjects[0]
        except:
            ref1 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[0]
            ref2 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[1]
            
        
        #Add in-line joint:
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        obj = 'joint_'+node1.label+'_'+node2.label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Inline(a,str(existingjoints), node1 ,node2, ref1, ref2)
        CustomView(a.ViewObject)
        a.ViewObject.Transparency = 85
        a.ViewObject.LineWidth = 1.00
        a.Label='joint: '+str(existingjoints)
                
        #Move the object to it's container            
        FreeCAD.ActiveDocument.getObjectsByLabel("slider: "+str(existingsliders))[0].addObject(a) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+str(existingjoints))[0]) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0]) 
   
        #Add a prismatic joint:
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        obj = 'joint_'+node1.label+'_'+node2.label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Prismatic(a,str(existingjoints),node2,node1)
        CustomView(a.ViewObject)
        a.ViewObject.Transparency = 85
        a.ViewObject.LineWidth = 1.00
        a.Label='joint: '+str(existingjoints) #rename the joint
  
        #Move the object to it's container            
        FreeCAD.ActiveDocument.getObjectsByLabel("slider: "+str(existingsliders))[0].addObject(a) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0])
         
        FreeCAD.ActiveDocument.recompute()  
                  
#########################################################################################################################################################################################################################################################               
    def AddAxialRotationJoint(self):
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
        try:
            ref1 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[0]
            ref2 = FreeCADGui.Selection.getSelectionEx()[3].SubObjects[0]
        except:
            ref1 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[0]
            ref2 = FreeCADGui.Selection.getSelectionEx()[2].SubObjects[1]
            
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        obj = 'joint_'+node1.label+'_'+node2.label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        Axialrotation(a,str(existingjoints),node1,node2,ref1,ref2)
        CustomView(a.ViewObject) 
        a.ViewObject.Transparency = 85
        a.ViewObject.LineWidth = 1.00
        #Move the object to it's container            
        FreeCAD.ActiveDocument.getObject('Axial_rotation_joints').addObject(a)
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+str(existingjoints))[0]) 
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0])
        #Add vectors to animate reaction forces:
        a.Label='joint: '+str(existingjoints) #rename the joint 
        FreeCAD.ActiveDocument.recompute()  
        
#########################################################################################################################################################################################################################################################               
    def AddLaw(self):
        existinglaws = 1
        for obj in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj.Label.startswith('law'):
                existinglaws = existinglaws+1
                
        obj = 'law_'+str(existinglaws)
        a = FreeCAD.ActiveDocument.addObject("App::FeaturePython",obj)
        ConstitutiveLaw(a, str(existinglaws))
        CustomView(a.ViewObject)
        #Move the object to it's container            
        FreeCAD.ActiveDocument.getObject("Constitutive_laws").addObject(a)        
        a.Label='law: '+str(existinglaws) 
        FreeCAD.ActiveDocument.recompute() 
        return a
        
#########################################################################################################################################################################################################################################################               
    def AddDeformableDisplacementJoint(self):
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
            
        existingjoints = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('joint'):
                existingjoints = existingjoints+1
                
        obj = 'joint_'+node1.label+'_'+node2.label
        a = FreeCAD.ActiveDocument.addObject("Part::FeaturePython",obj)
        DeformableDisplacement(a, existingjoints, node1, node2)
        a.Label='joint: '+str(existingjoints)
        CustomView(a.ViewObject)
        #Move the object to it's container                    
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+str(existingjoints))[0])
        a.addObject(FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+str(existingjoints))[0])
        FreeCAD.ActiveDocument.getObject("Deformable_displacement_joints").addObject(a)        
        FreeCAD.ActiveDocument.recompute()
              
#########################################################################################################################################################################################################################################################               
    def PlotNode(self, node, expression):
        Plotnode(node,expression)

#########################################################################################################################################################################################################################################################               
    def PlotAbstract(self, node):
        Plotabstract(node)
        
#########################################################################################################################################################################################################################################################               
    def PlotDrive(self, obj):
        if(obj.Label.startswith("drive")):
            PlotDrives(FreeCADGui.Selection.getSelection()[0])    

        if(obj.Label.startswith("function")):
            PlotScalars()    
        
#########################################################################################################################################################################################################################################################                       
    def PlotJoint(self, joint, expression):
        Plotjoint(joint,expression)
        
#########################################################################################################################################################################################################################################################               
    def RestoreScene(self):
        ani.restore()
        
#########################################################################################################################################################################################################################################################               
    def StartAnimation(self):
        ani.start()
        #ani.startwithnodeplot(1, "P(t)")
        
#########################################################################################################################################################################################################################################################               
    def StopAnimation(self):
        ani.stop()
        
#########################################################################################################################################################################################################################################################               
#Execute MBDyn using the input file generated:
    def Run(self):
        # Open the FreeCAD text file and create a text document to be used as input file for MBDyn:
        __dir1__ = tempfile.gettempdir()#'C:/Users/Equipo/AppData/Local/Temp'  
        text = FreeCAD.ActiveDocument.getObject("input_file").Text
        #file = open(__dir__ + '/MBDyn/MBDynCase.mbd','w') 
        file = open(__dir1__ + '/MBDynCase.mbd','w') 
        file.write(text)        
        file.close()
        # Execute MBDyn:
        msgBox = QtGui.QMessageBox()
        if platform == "linux" or platform == "linux2":            
            process = subprocess.Popen(["mbdyn", "-f", __dir1__+"/MBDynCase.mbd"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = process.communicate()           
            if(len(err)>0):            
                msgBox.setText("MBDyn says:")
                msgBox.setInformativeText(err.decode('utf8'))
                msgBox.exec_()
            else:
                msgBox.setText("SUCCESSFUL SIMULATION!")
                msgBox.setInformativeText(out[:555].decode('utf8'))
                msgBox.exec_()
                
        if platform == "win32": 
            process = subprocess.Popen([__dir__+"\mbdyn-1.7.3-win32\mbdyn.exe","-f", __dir1__+"/MBDynCase.mbd"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = process.communicate()           
            if(len(err)>0):            
                msgBox.setText("MBDyn says:")
                msgBox.setInformativeText(err.decode('utf8'))
                msgBox.exec_()
            else:
                msgBox.setText("SUCCESSFUL SIMULATION!")
                msgBox.setInformativeText(out[:555].decode('utf8'))
                msgBox.exec_()
                
#########################################################################################################################################################################################################################################################               
    def WriteInputFile(self):
        Writeinpfile()
        
#########################################################################################################################################################################################################################################################                       
    def AddGravity(self, BaseBody = "NONE"):#This method adds gravity to the simulation:
        existinggravities = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('gravity'):
                existinggravities = existinggravities+1

        #Add gravity:            
        obj = FreeCAD.ActiveDocument.addObject("Part::FeaturePython","gravity")
        obj.Label = 'gravity: ' + str(existinggravities)
        Gravity(obj, BaseBody, str(existinggravities))
        if(BaseBody!="NONE"):    
            BaseBody.Label = 'object: gravity: ' + str(existinggravities)
            
        CustomView(obj.ViewObject)
        #Move the gravity object to it's container:
        FreeCAD.ActiveDocument.getObject("Forces").addObject(obj)
        if(BaseBody=="NONE"):
            obj.addObject(FreeCAD.ActiveDocument.getObjectsByLabel('z: gravity: '+ obj.label)[0])
        
        FreeCAD.ActiveDocument.recompute()
        return obj
            
#########################################################################################################################################################################################################################################################                       
    def AutoReOrganize(self):#In case a joint has been deleted, use this method to rename all the remaining joints, for consistency:
        number_of_joints = 0
        joints = []
        #Count number of joints. This has to be counted from the FeeCAD document because some animations may not have any joint, so that the "MBDynCase.jnt" file may not exist or may correspond to another simulation             
        for obj in FreeCAD.ActiveDocument.Objects:
            if(obj.Label.startswith('joint:')):
                number_of_joints = number_of_joints + 1 
                joints.append(int(obj.label))

        if max(joints) == number_of_joints:                
            QtGui.QMessageBox.information(None,'Ok.','There are no joints naming inconsistencies.')
        else:
            QtGui.QMessageBox.information(None,'Error.','There are naming inconsistencies. Joints will be re-named to keep consistency.')
            labbel = 1

            for obj in FreeCAD.ActiveDocument.Objects:
                if(obj.Label.startswith('joint:')):
                    if obj.joint=='drive hinge':                       
                        FreeCAD.ActiveDocument.getObjectsByLabel('scalar function: '+str(obj.label))[0].Label = 'scalar function: '+str(labbel)                        
                    if obj.joint=='revolute pin' or obj.joint=='revolute hinge' or obj.joint=='in line' or obj.joint=='axial rotation' or obj.joint=='drive hinge' or obj.joint=='deformable displacement' or obj.joint=='revolute rotation':
                        FreeCAD.ActiveDocument.getObjectsByLabel('z: joint: '+str(obj.label))[0].Label = 'z: joint: '+str(labbel)
                        FreeCAD.ActiveDocument.getObjectsByLabel('jf: '+str(obj.label))[0].Label = 'jf: '+str(labbel)

                    if obj.joint=='clamp' or obj.joint=='spherical hinge' or obj.joint=='prismatic':                        
                        FreeCAD.ActiveDocument.getObjectsByLabel('jf: '+str(obj.label))[0].Label = 'jf: '+str(labbel)

#                    else:
#                        FreeCAD.ActiveDocument.getObjectsByLabel('x: joint: '+str(obj.label))[0].Label = 'x: joint: '+str(labbel)
#                        FreeCAD.ActiveDocument.getObjectsByLabel('y: joint: '+str(obj.label))[0].Label = 'y: joint: '+str(labbel)
#                        FreeCAD.ActiveDocument.getObjectsByLabel('z: joint: '+str(obj.label))[0].Label = 'z: joint: '+str(labbel)
#                        FreeCAD.ActiveDocument.getObjectsByLabel('jf: '+str(obj.label))[0].Label = 'jf: '+str(labbel)
                        
                    obj.Label = 'joint: '+str(labbel)
                    obj.label = str(labbel)
                    labbel = int(random()*10000)

            labbel = 1

            for obj in FreeCAD.ActiveDocument.Objects:
                if(obj.Label.startswith('joint:')):
                    if obj.joint=='drive hinge':                       
                        FreeCAD.ActiveDocument.getObjectsByLabel('scalar function: '+str(obj.label))[0].Label = 'scalar function: '+str(labbel)                        
                    if obj.joint=='revolute pin' or obj.joint=='revolute hinge' or obj.joint=='in line' or obj.joint=='axial rotation' or obj.joint=='drive hinge' or obj.joint=='deformable displacement' or obj.joint=='revolute rotation':
                        FreeCAD.ActiveDocument.getObjectsByLabel('z: joint: '+str(obj.label))[0].Label = 'z: joint: '+str(labbel)
                        FreeCAD.ActiveDocument.getObjectsByLabel('jf: '+str(obj.label))[0].Label = 'jf: '+str(labbel)
                    if obj.joint=='clamp' or obj.joint=='spherical hinge' or obj.joint=='prismatic':    
                        FreeCAD.ActiveDocument.getObjectsByLabel('jf: '+str(obj.label))[0].Label = 'jf: '+str(labbel)
                        
                    obj.Label = 'joint: '+str(labbel)
                    obj.label = str(labbel)
                    labbel = labbel +1
                    
            for obj in FreeCAD.ActiveDocument.Objects:                                        
                if obj.Label.startswith('joint: ') or obj.Label.startswith('force: '):               
                    FreeCAD.ActiveDocument.getObject(obj.Name).purgeTouched()
                    
        return    
       
#########################################################################################################################################################################################################################################################               
    def Inspect(self, BaseBody, density):
        density = density/(1000.0**3)
        precision = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)
        #obtain object's volume in mm^3:      
        volume = FreeCAD.Units.Quantity(BaseBody.Shape.Volume,FreeCAD.Units.Unit('mm^3')) 
            #calculate object's mass, in kilograms:
        mass = FreeCAD.Units.Quantity(volume*density,FreeCAD.Units.Unit('kg'))
            #Returns moments of inertia divided by density:
        inertia = BaseBody.Shape.Solids[0].MatrixOfInertia
            #Store inertia moments without mass in mm^2:
        iixx = FreeCAD.Units.Quantity(inertia.A[0],FreeCAD.Units.Unit('mm^5'))
        iiyy = FreeCAD.Units.Quantity(inertia.A[5],FreeCAD.Units.Unit('mm^5'))
        iizz = FreeCAD.Units.Quantity(inertia.A[10],FreeCAD.Units.Unit('mm^5'))
            #Compute inertia moments, in kg*mm^2:      
        ixx = FreeCAD.Units.Quantity(iixx*density,FreeCAD.Units.Unit('kg*mm^2'))
        iyy = FreeCAD.Units.Quantity(iiyy*density,FreeCAD.Units.Unit('kg*mm^2'))
        izz = FreeCAD.Units.Quantity(iizz*density,FreeCAD.Units.Unit('kg*mm^2'))
            #Compute absolute center of mass possition, relative to global frame:
        x = FreeCAD.Units.Quantity(BaseBody.Shape.Solids[0].CenterOfMass[0],FreeCAD.Units.Unit('mm'))
        y = FreeCAD.Units.Quantity(BaseBody.Shape.Solids[0].CenterOfMass[1],FreeCAD.Units.Unit('mm'))
        z = FreeCAD.Units.Quantity(BaseBody.Shape.Solids[0].CenterOfMass[2],FreeCAD.Units.Unit('mm'))
        
        length = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObject("Line").End[0],FreeCAD.Units.Unit('mm'))
        
        FreeCAD.ActiveDocument.cmx.X1=x+length
        FreeCAD.ActiveDocument.cmx.Y1=y
        FreeCAD.ActiveDocument.cmx.Z1=z
        FreeCAD.ActiveDocument.cmx.X2=x-length#+body.Label
        FreeCAD.ActiveDocument.cmx.Y2=y
        FreeCAD.ActiveDocument.cmx.Z2=z
        
        FreeCAD.ActiveDocument.cmy.X1=x
        FreeCAD.ActiveDocument.cmy.Y1=y+length
        FreeCAD.ActiveDocument.cmy.Z1=z
        FreeCAD.ActiveDocument.cmy.X2=x
        FreeCAD.ActiveDocument.cmy.Y2=y-length
        FreeCAD.ActiveDocument.cmy.Z2=z
        
        FreeCAD.ActiveDocument.cmz.X1=x
        FreeCAD.ActiveDocument.cmz.Y1=y
        FreeCAD.ActiveDocument.cmz.Z1=z+length
        FreeCAD.ActiveDocument.cmz.X2=x
        FreeCAD.ActiveDocument.cmz.Y2=y
        FreeCAD.ActiveDocument.cmz.Z2=z-length
        
        FreeCAD.ActiveDocument.recompute()
        QtGui.QMessageBox.information(None,'Physical properties:','Volume [cm^3]: '+str(round(volume.getValueAs('cm^3').Value,precision))+'\n\n'+'Mass [kg]: '+str(round(mass.Value,precision))+'\n\n'+'Center of mass:\n\n'+'    cmx [mm]: '+str(round(x.Value,precision))+'\n'+'    cmy [mm]: '+str(round(y.Value,precision))+'\n'+'    cmz [mm]: '+str(round(z.Value,precision))+'\n\n'+'Moments of inertia:\n\n'+'    ixx [kg*mm^2] :'+str(round(ixx.Value,precision))+'\n'+'    iyy [kg*mm^2] :'+str(round(iyy.Value,precision))+'\n'+'    izz [kg*mm^2] :'+str(round(izz.Value,precision))+'\n')

    #def RunMBDyn(self):#This method executes MBDyn using the input file previously generated
    #    subprocess.Popen(["mbdyn", "-f", __dir__+"/MBDyn/MBDynCase.mbd"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    def CreateWorld(self):#This method creates all the containers in the tree view and moves all the exiting objects into the "Bodies" container.
        if(len(FreeCAD.ActiveDocument.getObjectsByLabel("MBDyn_simulation"))==0):#Only if a world does not exist:
            if(len(FreeCAD.ActiveDocument.Objects)>0):            
                Createworld()#Adds all the goups and the global reference frame
                #Create a simulation and move it into the container:
                a = FreeCAD.ActiveDocument.addObject("App::FeaturePython","MBDyn")
                MBDyn(a)
                CustomView(a.ViewObject)
                FreeCAD.ActiveDocument.getObject("MBDyn_simulation").addObject(a) 
                #Create a simulation parameters object and move it to its container:
                b = FreeCAD.ActiveDocument.addObject("App::FeaturePython","Animation")        
                Animation_parameters(b)            
                CustomView(b.ViewObject)
                FreeCAD.ActiveDocument.getObject("MBDyn_simulation").addObject(b)
                #Create control data parameters object and move it to its container:
                c = FreeCAD.ActiveDocument.addObject("App::FeaturePython","ControlData")        
                ControlData(c)            
                CustomView(c.ViewObject)
                FreeCAD.ActiveDocument.getObject("MBDyn_simulation").addObject(c)
            else:
                QtGui.QMessageBox.information(None,'Error.','You need at least one solid object to create a simulation.')
        else:
            QtGui.QMessageBox.information(None,'Error.','A world has already been created.')
    



    def RandomColor(self, body):
            colorR, colorG, colorB = random(), random(), random()
            #b = FreeCADGui.Selection.getSelection()[0]            
            body.ViewObject.LineColor = (colorR,colorG,colorB)
            body.ViewObject.PointColor = (colorR,colorG,colorB)
            body.ViewObject.ShapeColor = (colorR,colorG,colorB)
            #if(len(FreeCAD.ActiveDocument.getObjectsByLabel("i: "+body.label))==1):
            #    FreeCAD.ActiveDocument.getObjectsByLabel("i: "+body.label)[0].ViewObject.TextColor = (colorR,colorG,colorB)

    def UpdateDynamicNode(self, objeto):#This method moves the coordinate system of a node to a new position and orientation:
        return
        #ManageAxes.MoveAxes(objeto)

    def UpdateJoint(self, objeto):
        return
        #ManageAxes.MoveAxes(objeto)
        #if(objeto.joint=='revolute pin'):
        #    node = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+joint.node)[0]
        #    #Get the joint possition:
        #    xc = joint.absolute_pin_position_X
        #    yc = joint.absolute_pin_position_Y
        #    zc = joint.absolute_pin_position_Z
            #Get the corresponding node possition:
        #    xcc = node.position_X
        #    ycc = node.position_Y
        #    zcc = node.position_Z
            #Remove  joint force vector and add a new one on the new possition:
        #    FreeCAD.ActiveDocument.removeObject(FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+joint.label)[0].Name)
        #    Forceaxes(joint)
            #Update the joint's offset:
        #    joint.relative_offset_X = xc - xcc
        #    joint.relative_offset_Y = yc - ycc
        #    joint.relative_offset_Z = zc - zcc         
                  
    def UpdateRigidBody(self, Body):#This method gets a reigid body (b) and updates it in case of: 
       #a) The shape of the original body (parametric CAD) has changed, 
       #b) The user has changed the rigid body's density
       #c) The user has moved the asociated structural node.
        #Body = FreeCAD.ActiveDocument.getObjectsByLabel(body)[0]
        label = Body.label
        #Get the new shape from the corresponding parametric body:
        shape = FreeCAD.ActiveDocument.getObjectsByLabel(label)[0].Shape
        #Update the rigid body's shape
        Body.Shape = shape
        #Get the new inertia matrix:
        inertia = shape.Solids[0].MatrixOfInertia
        #get the new density (defined by the user in the Material object):
        Body.density = FreeCAD.ActiveDocument.getObjectsByLabel("material: "+Body.label)[0].Material['Density']
        density = FreeCAD.Units.Quantity(float(Body.density.split(' ')[0])/1000.0**3,FreeCAD.Units.Unit('kg/mm^3'))#Convert density to the appropriate units, to calculate moments of inertia
        #get the new volume:
        volume = FreeCAD.Units.Quantity(shape.Volume,FreeCAD.Units.Unit('mm^3'))  
        #calculate the new object's mass, in kilograms:
        mass = FreeCAD.Units.Quantity(volume*density,FreeCAD.Units.Unit('kg'))
        #get the new inertia moments without mass:
        iixx = FreeCAD.Units.Quantity(inertia.A[0],FreeCAD.Units.Unit('mm^5'))
        iiyy = FreeCAD.Units.Quantity(inertia.A[5],FreeCAD.Units.Unit('mm^5'))
        iizz = FreeCAD.Units.Quantity(inertia.A[10],FreeCAD.Units.Unit('mm^5'))
        #compute new inertia moments, in kg*mm^2: 
        ixx = FreeCAD.Units.Quantity(iixx*density,FreeCAD.Units.Unit(2,1))
        iyy = FreeCAD.Units.Quantity(iiyy*density,FreeCAD.Units.Unit(2,1))
        izz = FreeCAD.Units.Quantity(iizz*density,FreeCAD.Units.Unit(2,1))
        #Compute new absolute center of mass, relative to global frame:
        cmx = FreeCAD.Units.Quantity(shape.Solids[0].CenterOfMass[0],FreeCAD.Units.Unit('mm'))
        cmy = FreeCAD.Units.Quantity(shape.Solids[0].CenterOfMass[1],FreeCAD.Units.Unit('mm'))
        cmz = FreeCAD.Units.Quantity(shape.Solids[0].CenterOfMass[2],FreeCAD.Units.Unit('mm'))
        Body.mass = str(mass)#Update mass
        #Update moments of inertia with mass:
        Body.ixx = str(ixx)
        Body.iyy = str(iyy)
        Body.izz = str(izz)
        #Update moments of inertia witout mass:
        Body.iixx = str(iixx)
        Body.iiyy = str(iiyy)
        Body.iizz = str(iizz)            
        #Update the absolute center of mass:
        Body.absolute_center_of_mass_X = cmx
        Body.absolute_center_of_mass_Y = cmy
        Body.absolute_center_of_mass_Z = cmz
            
            #get the corresponding node's absolute possition:  
        if(len(FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+label))==1):
            xcc = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+label)[0].position_X
            ycc = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+label)[0].position_Y
            zcc = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+label)[0].position_Z
            #Update the body's relative center of mass position:
            Body.relative_center_of_mass_X = Body.absolute_center_of_mass_X-xcc
            Body.relative_center_of_mass_Y = Body.absolute_center_of_mass_Y-ycc
            Body.relative_center_of_mass_Z = Body.absolute_center_of_mass_Z-zcc
        else:#If there is no structural node asociated to the body, issue an error:
            QtGui.QMessageBox.information(None,'Warning.','No structural node asociated to this body. Relative center of mass cannot be calculated.')
      
