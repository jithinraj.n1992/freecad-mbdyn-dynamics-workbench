# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
"""
This class receives a base node and a non-parametric base body, and creates a dummy node positioned at the base body's center of mass, and attached to the base node.
A dummy node does not own any dregree of freedom. It gets its possition and orientation from its base node, and allows measuring the possition and orientation of a ponit attached to a dynamic node.  

The syntax is:

structural: <label>,
            dummy,
            <base_node>,
            offset,
            <offset_relative_to_the_base_node>,
            <orientation_relative_to_the_base_node>;
Where:
<label> is an integer number that identifies the node. Label the base body in FreeCAD with a unique integer number to identify it's node, before you call this class.
<base_node> is the label of the structural dynamic node to which the dummy node is attached.
<offset_relative_to_the_base_node>, is the offset (z,y,z) of the dummy node relative to its structural dynamic node.
<orientation_relative_to_the_base_node> is the initial orientation of the dummy node relative to the orientation of its structural dynamic node. I use "eye" by default, to assume the same orientation as the structural dynamic node.
"""

import FreeCAD
import Draft

class Dummynode: 
    def __init__(self, obj, baseNode, baseBody):
        
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)
        
        #Here, I create a dummy node at the center of mass of the base body CAD object (baseBody). 
        #I first obtain the center of mass of the baseBody (x,y,z):
                
        try:    
            x = FreeCAD.Units.Quantity(baseBody.Shape.Solids[0].CenterOfMass[0],FreeCAD.Units.Unit('mm'))
            y = FreeCAD.Units.Quantity(baseBody.Shape.Solids[0].CenterOfMass[1],FreeCAD.Units.Unit('mm'))
            z = FreeCAD.Units.Quantity(baseBody.Shape.Solids[0].CenterOfMass[2],FreeCAD.Units.Unit('mm'))  
            length = (baseBody.Shape.BoundBox.XLength+baseBody.Shape.BoundBox.YLength+baseBody.Shape.BoundBox.ZLength)/6 # Calculate the body characteristic length. Will be used to size the arrows that represent the node.
            
        except:#If a point, instead of an object, was selected as reference:
            x = FreeCAD.Units.Quantity(baseBody.X,FreeCAD.Units.Unit('mm'))
            y = FreeCAD.Units.Quantity(baseBody.Y,FreeCAD.Units.Unit('mm'))
            z = FreeCAD.Units.Quantity(baseBody.Z,FreeCAD.Units.Unit('mm'))
            length = FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/10
                            
        #Then, I obtain the offset (x1,y1,z1), this is, the possition of the dummy node relative to the baseNode
        x1 = x-baseNode.absolute_position_X
        y1 = y-baseNode.absolute_position_Y
        z1 = z-baseNode.absolute_position_Z
        
        #By now, al the nodes are created with initial orientation (yaw=0, pitch=0, roll=0). Give the node an initial absolute orienation in Euler angles:  
        yaw = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('deg'))
        pitch = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('deg'))
        roll = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('deg')) 

        #Give the object the ability to contain other objetcs:
        obj.addExtension("App::GroupExtensionPython", self)        

        #Give the object its properties:        
        obj.addProperty("App::PropertyString","type","dummy node","type",1).type = 'dummy'
        obj.addProperty("App::PropertyString","label","dummy node","label",1).label = baseBody.Label
        obj.addProperty("App::PropertyString","plugin variables","dummy node","plugin variables",1).plugin_variables = "none"
        
        #Node possition:        
        obj.addProperty("App::PropertyDistance","absolute position_X","absolute possition","absolute position X",1).absolute_position_X = round(x,precission)
        obj.addProperty("App::PropertyDistance","absolute position_Y","absolute possition","absolute position Y",1).absolute_position_Y = round(y,precission)
        obj.addProperty("App::PropertyDistance","absolute position_Z","absolute possition","absolute position Z",1).absolute_position_Z = round(z,precission)
        
        #Base node label:
        obj.addProperty("App::PropertyString","base node","dummy node","base node",1).base_node = baseNode.label
        
        #Offset relative to dynamic node:
        obj.addProperty("App::PropertyString","relative offset X","relative offset","relative offset X",1).relative_offset_X = str(round(x1.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","relative offset Y","relative offset","relative offset Y",1).relative_offset_Y = str(round(y1.getValueAs('m').Value,precission))+" m"       
        obj.addProperty("App::PropertyString","relative offset Z","relative offset","relative offset Z",1).relative_offset_Z = str(round(z1.getValueAs('m').Value,precission))+" m"
        
        #Initial orientation:                
        obj.addProperty("App::PropertyAngle","yaw","absolute orientation","yaw").yaw = yaw    
        obj.addProperty("App::PropertyAngle","pitch","absolute orientation","pitch").pitch = pitch    
        obj.addProperty("App::PropertyAngle","roll","absolute orientation","roll").roll = roll    
                
        obj.Proxy = self
        
        #Add the coordinate system and an system to the GUI. The coordinate system represents the position of the node in space:
        #length = (baseBody.Shape.BoundBox.XLength+baseBody.Shape.BoundBox.YLength+baseBody.Shape.BoundBox.ZLength)/6 # Calculate the body characteristic length. Will be used to size the arrows that represent the node.
        p1 = FreeCAD.Vector(0, 0, 0)
        #Add x vector of the coordinate system:
        p2 = FreeCAD.Vector(length, 0, 0)
        l = Draft.makeLine(p1, p2) 
        l.Label = 'x: structural: '+ baseBody.Label          
        l.ViewObject.LineColor = (1.00,0.00,0.00)
        l.ViewObject.PointColor = (1.00,0.00,0.00)
        l.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        l.ViewObject.EndArrow = True
        l.ViewObject.ArrowType = u"Arrow"
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        l.ViewObject.ArrowSize = str(length/15)+' mm'
        #Add y vector of the coordinate system:
        p2 = FreeCAD.Vector(0, length, 0)
        l = Draft.makeLine(p1, p2)        
        l.Label = 'y: structural: '+ baseBody.Label        
        l.ViewObject.LineColor = (0.00,1.00,0.00)
        l.ViewObject.PointColor = (0.00,1.00,0.00)
        l.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        l.ViewObject.EndArrow = True
        l.ViewObject.ArrowType = u"Arrow"
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        l.ViewObject.ArrowSize = str(length/15)+' mm'        
        #Add z vector of the coordinate system:
        p2 = FreeCAD.Vector(0, 0, 0+length)
        l = Draft.makeLine(p1, p2)        
        l.Label = 'z: structural: '+ baseBody.Label                        
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)
        l.Placement=FreeCAD.Placement(FreeCAD.Vector(x,y,z), FreeCAD.Rotation(FreeCAD.Vector(0,0,1),0), FreeCAD.Vector(0,0,0))
        l.ViewObject.EndArrow = True  
        l.ViewObject.ArrowType = u"Arrow"
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        l.ViewObject.ArrowSize = str(length/15)+' mm'                                     
                               
    def execute(self, fp):
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)
        #Calculate the new offset:
        node = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.base_node)[0]
        #get te position (this is not expected to change)
        x = fp.absolute_position_X.getValueAs("m").Value
        y = fp.absolute_position_Y.getValueAs("m").Value
        z = fp.absolute_position_Z.getValueAs("m").Value

        #Calculate the offset        
        x1 = x - node.absolute_position_X.getValueAs("m").Value
        y1 = y - node.absolute_position_Y.getValueAs("m").Value
        z1 = z - node.absolute_position_Z.getValueAs("m").Value
                
        #Update the offset:
        fp.relative_offset_X = str(round(x1,precission))+" m"
        fp.relative_offset_Y = str(round(y1,precission))+" m"
        fp.relative_offset_Z = str(round(z1,precission))+" m"
        
        FreeCAD.Console.PrintMessage("DUMMY NODE: " +fp.label+" successful recomputation...\n")   