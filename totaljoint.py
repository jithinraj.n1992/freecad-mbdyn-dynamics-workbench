# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################

#from FreeCAD import Units
import FreeCAD
import Draft

class Totaljoint:
    def __init__(self, obj, label, node1, node2):
        
        precission = int(FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].precision)
        
        obj.addExtension("App::GroupExtensionPython", self) 

        x = node1.absolute_position_X
        y = node1.absolute_position_Y
        z = node1.absolute_position_Z     
        
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","total joint","label",1).label = label                        
        obj.addProperty("App::PropertyString","joint","total joint","joint",1).joint = 'total joint'
        obj.addProperty("App::PropertyString","plugin variables","total joint","plugin variables",1).plugin_variables = "none"
        obj.addProperty("App::PropertyString","absolute position X","absolute position","absolute position X",1).absolute_position_X = str(round(x.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Y","absolute position","absolute position Y",1).absolute_position_Y = str(round(y.getValueAs('m').Value,precission))+" m"
        obj.addProperty("App::PropertyString","absolute position Z","absolute position","absolute position Z",1).absolute_position_Z = str(round(z.getValueAs('m').Value,precission))+" m"
            
        obj.addProperty("App::PropertyString","node 1","node 1","node 1",1).node_1 = node1.label
        obj.addProperty("App::PropertyString","relative offset 1","node 1","relative offset 1",1).relative_offset_1 = "null"
        obj.addProperty("App::PropertyString","rel pos orientation 1","node 1","rel pos orientation 1",1).rel_pos_orientation_1 = "1, 1.,0.,0., 2, guess"
        obj.addProperty("App::PropertyString","rel rot orientation 1","node 1","rel rot orientation 1",1).rel_rot_orientation_1 = "1, 1.,0.,0., 2, guess"
        
        obj.addProperty("App::PropertyString","node 2","node 2","node 2",1).node_2 = node2.label
        obj.addProperty("App::PropertyString","relative offset 2","node 2","relative offset 2",1).relative_offset_2 = "null"
        obj.addProperty("App::PropertyString","rel pos orientation 2","node 2","rel pos orientation 2",1).rel_pos_orientation_2 = "1, 1.,0.,0., 2, guess"
        obj.addProperty("App::PropertyString","rel rot orientation 2","node 2","rel rot orientation 2",1).rel_rot_orientation_2 = "1, 1.,0.,0., 2, guess"         

        obj.addProperty("App::PropertyEnumeration","position_status","position constraint","position_status")
        obj.position_status=['inactive', 'active', 'position', 'velocity']
        obj.addProperty("App::PropertyEnumeration","imposed_position_axis","position constraint","imposed_position_axis")
        obj.imposed_position_axis=['1', '2', '3']
        obj.addProperty("App::PropertyString","imposed relative position","position constraint","imposed relative position").imposed_relative_position = "null"
        obj.addProperty("App::PropertyString","imposed position modifier","position constraint","imposed position modifier").imposed_position_modifier = "*1."         

        obj.addProperty("App::PropertyEnumeration","orientation_status","orientation constraint","orientation_status")
        obj.orientation_status=['inactive', 'active', 'rotation', 'angular velocity']
        obj.addProperty("App::PropertyEnumeration","imposed_orientation_axis","orientation constraint","imposed_orientation_axis")
        obj.imposed_orientation_axis=['1', '2', '3']
        obj.addProperty("App::PropertyString","imposed relative rotation","orientation constraint","imposed relative rotation").imposed_relative_rotation = "null"        
        obj.addProperty("App::PropertyString","imposed rotation modifier","orientation constraint","imposed rotation modifier").imposed_rotation_modifier = "*1."         
              
        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","animation","frame")
        obj.frame=['global','local']        
        
        obj.addProperty("App::PropertyString","force vector multiplier","animation","force vector multiplier").force_vector_multiplier = '1'
        
        obj.addProperty("App::PropertyString","structural dummy","animation","structural dummy").structural_dummy = '1'

        obj.Proxy = self   
             
        
        #Add the vector to visualize reaction forces
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        p1 = FreeCAD.Vector(x,y,z)        
        p2 = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)    
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        d.Label = "jf: "+ label 
                
    def execute(self, fp):
            FreeCAD.Console.PrintMessage("TOTAL JOINT: " +fp.label+" successful recomputation...\n")
