# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################


from matplotlib import pyplot as plt
import numpy as np
import FreeCAD, FreeCADGui
import tempfile
from sys import platform
import subprocess
import os

class PlotScalars:  
    def __init__(self):

        MBDyn = FreeCAD.ActiveDocument.getObject("MBDyn")#Get the simulation scripted object                                     

        listlines = []
                        
        listlines.append('begin: data;\n')
        listlines.append('    problem: initial value;\n')
        listlines.append('end: data;\n\n') 
        
        aux = float(FreeCADGui.Selection.getSelection()[0].final_value)/2
        inittime = float(FreeCADGui.Selection.getSelection()[0].initial_value) 
        endtime = float(FreeCADGui.Selection.getSelection()[0].final_value) 
        
        listlines.append('set: real XL = '+ str(inittime) +';\n')
        listlines.append('set: real XU = '+ str(endtime) +';\n')
        #listlines.append('set: real dX = '+ str(timestep) +';\n\n')

        #listlines.append('set: real XL = '+ MBDyn.initial_time[:-2] +';\n')
        #listlines.append('set: real XU = '+ MBDyn.final_time[:-2] +';\n')
        listlines.append('set: real dX = '+ MBDyn.time_step[:-2] +';\n\n')
        
        
        listlines.append('begin: initial value;\n')    
        listlines.append('    initial time:   XL;\n')
        listlines.append('    final time:     XU;\n')
        listlines.append('    time step:      dX;\n')
        listlines.append('    max iterations: 10;\n')
        listlines.append('    tolerance:      1.e-6;\n')
        listlines.append('    derivatives coefficient:      auto;\n')        
        listlines.append('end: initial value;\n\n')       
        
        listlines.append('begin: control data;\n')
        listlines.append('    abstract nodes: 2;\n')
        listlines.append('    genels: 2;\n')              
        listlines.append('end: control data;\n\n')

          
        listlines.append('scalar function: "function:1",\n')
        listlines.append(FreeCADGui.Selection.getSelection()[0].type.split(':')[0]+',\n')
        if FreeCADGui.Selection.getSelection()[0].extrapolation=="do not extrapolate":
            listlines.append(FreeCADGui.Selection.getSelection()[0].extrapolation+',\n')
        
        listlines.append(FreeCAD.ActiveDocument.getObjectsByLabel("points: "+FreeCADGui.Selection.getSelection()[0].label)[0].Text + ';\n\n')    

        listlines.append('set: integer NoAbs_X = 1;\n')
        listlines.append('set: integer NoAbs_Y = 2;\n')
        listlines.append('set: integer GeClamp_NoAbs_X = 1;\n')
        listlines.append('set: integer GeClamp_NoAbs_Y = 2;\n\n')

        listlines.append('begin: nodes;\n')
        listlines.append('    abstract: NoAbs_X, algebraic, value, XL;\n')
        listlines.append('    abstract: NoAbs_Y, algebraic, value, model::sf::function:1(XL);\n')                    
        listlines.append('end: nodes;\n\n')          
        
        listlines.append('set: [dof, X, NoAbs_X, abstract, algebraic];\n\n')
        
        listlines.append('begin: elements;\n')      
        listlines.append('    genel: GeClamp_NoAbs_X,\n')  
        listlines.append('        clamp,\n')  
        listlines.append('        NoAbs_X, abstract,\n')  
        listlines.append('        string, "Time+XL";\n\n')  
                      
        listlines.append('    genel: GeClamp_NoAbs_Y,\n')
        listlines.append('        clamp,\n')
        listlines.append('        NoAbs_Y, abstract,\n')
        listlines.append('        string, "model::sf::function:1(X)";\n\n')    
        listlines.append('end: elements;\n')
        #Add all the lines to the FreeCAD text document: 
        FreeCAD.ActiveDocument.getObjectsByLabel("input_file_aux")[0].Text = ' '.join(listlines)
        
        # Open the FreeCAD text file and create a text document to be used as input file for MBDyn:
        __dir1__ = tempfile.gettempdir()#'C:/Users/Equipo/AppData/Local/Temp'
        __dir__ = os.path.dirname(__file__)
        text = FreeCAD.ActiveDocument.getObject("input_file_aux").Text
        #file = open(__dir__ + '/MBDyn/MBDynCase.mbd','w') 
        file = open(__dir1__ + '/MBDynCase.mbd','w') 
        file.write(text)        
        file.close()
        # Execute MBDyn:
        if platform == "linux" or platform == "linux2":            
            p = subprocess.Popen(["mbdyn", "-f", __dir1__+"/MBDynCase.mbd"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)  
            p.wait()
                
        if platform == "win32": 
            p = subprocess.Popen([__dir__+"\mbdyn-1.7.3-win32\mbdyn.exe","-f", __dir1__+"/MBDynCase.mbd"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)   
            p.wait()

        data = np.genfromtxt(__dir1__ + '/MBDynCase.abs', delimiter=' ')
        
        aux = data[:,0]
        aux1 = data[:,1]
        xs = []
        ys = []        
        
        for d in range(len(aux)):
            if aux[d] == 1:
                xs.append(aux1[d])
                
            if aux[d] == 2:
                ys.append(aux1[d])
                
        fig, ax = plt.subplots()
        ax.plot(xs, ys)
        
        ax.set(xlabel='x', ylabel='f(x)', title = FreeCADGui.Selection.getSelection()[0].Label)
        ax.grid()
        
        plt.show()



